#ifndef HARP_FUNCTION_WRAPPER_HPP
#define HARP_FUNCTION_WRAPPER_HPP

extern "C" {
#include <lua.h>
}

struct FunctionWrapper {
    bool isLua;
    union {
        int luaFunc;
        void (*cFunc)();
    };
};

#endif
