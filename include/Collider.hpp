#ifndef HARP_COLLIDER_HPP
#define HARP_COLLIDER_HPP

#include "harpMath.hpp"

enum ColliderType {
    BOX_COLLIDER,
    LINE_COLLIDER
};

struct Collider {
    ColliderType type;
    Vec<2, double> delta;
};

#endif
