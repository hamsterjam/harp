#ifndef HARP_LUA_ENTITY_INTERFACE_HPP
#define HARP_LUA_ENTITY_INTERFACE_HPP

extern "C" {
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
}

#include "EntityTable.hpp"
#include "LuaHelper.hpp"

namespace LuaEntityInterface {
    //
    // Load
    //
    void load(lua_State* L);
    void addTable(lua_State* L, EntityTable& table, const char* name);
    template <typename T>
    void addComponent(lua_State* L, Component<T> comp, const char* name);

    //
    // Push
    //
    void pushEntityTable(lua_State* L, EntityTable& table);
    void pushEntity(lua_State* L, Entity ent);
    template <typename T>
    void pushComponent(lua_State* L, Component<T> comp);

    //
    // Check
    //
    EntityTable& checkEntityTable(lua_State* L, int arg);
    Entity checkEntity(lua_State* L, int arg);
    template <typename T>
    Component<T> checkComponent(lua_State* L, int arg);
}

namespace LuaFunction {
    namespace entitytable {
        int create(lua_State* L);
        int owns(lua_State* L);
        int has(lua_State* L);
        int get(lua_State* L);
        int view(lua_State* L);
        int set(lua_State* L);
        int apply(lua_State* L);
        int parent(lua_State* L);
        int orphan(lua_State* L);
    }

    namespace component {
        template <typename T> int owns(lua_State* L);
        template <typename T> int has(lua_State* L);
        template <typename T> int get(lua_State* L);
        template <typename T> int view(lua_State* L);
        template <typename T> int set(lua_State* L);
    }
}

namespace LuaNames {
    namespace Types {
        static constexpr const char* entitytable = "harp.entitytable";
        static constexpr const char* entity      = "harp.entity";
        static constexpr const char* component   = "harp.component";
    }
    namespace Values {
        static constexpr const char* entitytableStore = "table";
        static constexpr const char* componentStore   = "comp";

        namespace entitytable {
            static constexpr const char* create = "create";
            static constexpr const char* owns   = "owns";
            static constexpr const char* has    = "has";
            static constexpr const char* get    = "get";
            static constexpr const char* view   = "view";
            static constexpr const char* set    = "set";
            static constexpr const char* apply  = "apply";
            static constexpr const char* parent = "parent";
            static constexpr const char* orphan = "orphan";
        }

        namespace component {
            static constexpr const char* data = "_data";
            static constexpr const char* owns = "_owns";
            static constexpr const char* has  = "_has";
            static constexpr const char* get  = "_get";
            static constexpr const char* view = "_view";
            static constexpr const char* set  = "_set";
        }
    }
}

template <typename T>
void LuaEntityInterface::addComponent(lua_State* L, Component<T> comp, const char* name) {
    lua_getglobal(L, LuaNames::Values::componentStore);
    LuaEntityInterface::pushComponent<T>(L, comp);
    lua_setfield(L, -2, name);
    lua_pop(L, 1);
}

template <typename T>
void LuaEntityInterface::pushComponent(lua_State* L, Component<T> comp) {
    lua_newtable(L);

    auto ret = (Component<T>*) lua_newuserdata(L, sizeof(Component<T>));
    *ret = comp;
    luaL_getmetatable(L, LuaNames::Types::component);
    lua_setmetatable(L, -2);

    lua_setfield(L, -2, LuaNames::Values::component::data);

    lua_pushcfunction(L, LuaFunction::component::owns<T>); lua_setfield(L, -2, LuaNames::Values::component::owns);
    lua_pushcfunction(L, LuaFunction::component::has<T>);  lua_setfield(L, -2, LuaNames::Values::component::has);
    lua_pushcfunction(L, LuaFunction::component::get<T>);  lua_setfield(L, -2, LuaNames::Values::component::get);
    lua_pushcfunction(L, LuaFunction::component::view<T>); lua_setfield(L, -2, LuaNames::Values::component::view);
    lua_pushcfunction(L, LuaFunction::component::set<T>);  lua_setfield(L, -2, LuaNames::Values::component::set);
}

template <typename T>
Component<T> LuaEntityInterface::checkComponent(lua_State* L, int arg) {
    luaL_checktype(L, arg, LUA_TTABLE);
    lua_getfield(L, arg, LuaNames::Values::component::data);
    auto component = (Component<T>*) luaL_checkudata(L, -1, LuaNames::Types::component);
    lua_pop(L, 1);
    return *component;
}

//
// Functions for harp.component
//

template <typename T>
int LuaFunction::component::owns(lua_State* L) {
    EntityTable& table = LuaEntityInterface::checkEntityTable(L,  1);
    Entity       ent   = LuaEntityInterface::checkEntity(L,       2);
    Component<T> comp  = LuaEntityInterface::checkComponent<T>(L, 3);

    if (lua_gettop(L) >= 4) {
        bool value = lua_toboolean(L, 4);
        table.owns(ent, comp) = value;
    }

    lua_pushboolean(L, table.owns(ent, comp));
    return 1;
}

template <typename T>
int LuaFunction::component::has(lua_State* L) {
    EntityTable& table = LuaEntityInterface::checkEntityTable(L,  1);
    Entity       ent   = LuaEntityInterface::checkEntity(L,       2);
    Component<T> comp  = LuaEntityInterface::checkComponent<T>(L, 3);

    lua_pushboolean(L, table.has(ent, comp));
    return 1;
}

template <>
int LuaFunction::component::get<void>(lua_State* L);

template <typename T>
int LuaFunction::component::get(lua_State* L) {
    EntityTable& table = LuaEntityInterface::checkEntityTable(L,  1);
    Entity       ent   = LuaEntityInterface::checkEntity(L,       2);
    Component<T> comp  = LuaEntityInterface::checkComponent<T>(L, 3);

    if (lua_gettop(L) >= 4) {
        T value = LuaHelper::check<T>(L, 4);
        table.get(ent, comp) = value;
    }

    LuaHelper::push(L, table.get(ent, comp));
    return 1;
}

template <>
int LuaFunction::component::view<void>(lua_State* L);

template <typename T>
int LuaFunction::component::view(lua_State* L) {
    EntityTable& table = LuaEntityInterface::checkEntityTable(L,  1);
    Entity       ent   = LuaEntityInterface::checkEntity(L,       2);
    Component<T> comp  = LuaEntityInterface::checkComponent<T>(L, 3);

    LuaHelper::push(L, table.view(ent, comp));
    return 1;
}

template <>
int LuaFunction::component::set<void>(lua_State* L);

template <typename T>
int LuaFunction::component::set(lua_State* L) {
    EntityTable& table = LuaEntityInterface::checkEntityTable(L,  1);
    Entity       ent   = LuaEntityInterface::checkEntity(L,       2);
    Component<T> comp  = LuaEntityInterface::checkComponent<T>(L, 3);
    T            value = LuaHelper::check<T>(L,                   4);

    table.set(ent, comp, value);
    return 0;
}

#endif
