#ifndef HARP_LUA_HELPER_HPP
#define HARP_LUA_HELPER_HPP

#include <cstring>
#include <type_traits>

extern "C" {
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
}

#include "Collider.hpp"
#include "harpMath.hpp"

namespace LuaHelper {

    void load(lua_State* L);
    void loadHarp(lua_State* L);

    int weakRef(lua_State* L);
    void pushWeakRef(lua_State* L, int ref);

    //
    // template <typename T>
    // void push(lua_State* L, T value);
    //

    template <typename T>
    inline typename std::enable_if_t<std::is_same_v<T, bool>, void>
    push(lua_State* L, T value) {
        lua_pushboolean(L, value);
    }

    template <typename T>
    inline typename std::enable_if_t<std::is_integral_v<T> && !std::is_same_v<T, bool>, void>
    push(lua_State* L, T value) {
        lua_pushinteger(L, value);
    }

    template <typename T>
    inline typename std::enable_if_t<std::is_floating_point_v<T>, void>
    push(lua_State* L, T value) {
        lua_pushnumber(L, value);
    }

    template <typename T>
    inline typename std::enable_if_t<std::is_same_v<T, const char*>, void>
    push(lua_State* L, T value) {
        lua_pushstring(L, value);
    }

    template <typename T>
    inline typename std::enable_if_t<isVec<T>, void>
    push(lua_State* L, T value) {
        lua_createtable(L, T::dim, 0);

        for (int i = 0; i < T::dim; ++i) {
            push<typename T::type>(L, value[i]);
            lua_seti(L, -2, i+1);
        }
    }

    template <typename T>
    inline typename std::enable_if_t<std::is_same_v<T, Collider>, void>
    push(lua_State* L, T value) {
        lua_newtable(L);
        lua_getglobal(L, "Collider");
        lua_setmetatable(L, -2);

        switch (value.type) {
            case (ColliderType::BOX_COLLIDER):
                lua_pushstring(L, "box");
                break;
            case (ColliderType::LINE_COLLIDER):
                lua_pushstring(L, "line");
                break;
        }
        lua_setfield(L, -2, "colltype");

        LuaHelper::push(L, value.delta);
        lua_setfield(L, -2, "delta");
    }

    //
    // template <typename T>
    // T check(lua_State* L, int arg);
    //

    template <typename T>
    inline typename std::enable_if_t<std::is_same_v<T, bool>, T>
    check(lua_State* L, int arg) {
        return lua_toboolean(L, arg);
    }

    template <typename T>
    inline typename std::enable_if_t<std::is_integral_v<T> && !std::is_same_v<T, bool>, T>
    check(lua_State* L, int arg) {
        return luaL_checkinteger(L, arg);
    }

    template <typename T>
    inline typename std::enable_if_t<std::is_floating_point_v<T>, T>
    check(lua_State* L, int arg) {
        return luaL_checknumber(L, arg);
    }

    template <typename T>
    inline typename std::enable_if_t<std::is_same_v<T, const char*>, T>
    check(lua_State* L, int arg) {
        return luaL_checkstring(L, arg);
    }

    template <typename T>
    inline typename std::enable_if_t<isVec<T>, T>
    check(lua_State* L, int arg) {
        luaL_checktype(L, arg, LUA_TTABLE);

        T ret;
        for (int i = 0; i < T::dim; ++i) {
            lua_geti(L, arg, i+1);
            ret[i] = check<typename T::type>(L, -1);
            lua_pop(L, 1);
        }
        return ret;
    }

    template <typename T>
    inline typename std::enable_if_t<std::is_same_v<T, Collider>, T>
    check(lua_State* L, int arg) {
        Collider ret;
        luaL_checktype(L, arg, LUA_TTABLE);

        lua_getfield(L, arg, "colltype");
        const char* type = luaL_checkstring(L, -1);
        if      (!strcmp(type, "box"))  ret.type = ColliderType::BOX_COLLIDER;
        else if (!strcmp(type, "line")) ret.type = ColliderType::LINE_COLLIDER;
        lua_pop(L, 1);

        lua_getfield(L, arg, "delta");
        ret.delta = LuaHelper::check<Vec<2, double>>(L, -1);
        lua_pop(L, 1);

        return ret;
    }

    //
    // Global getters
    //

    template <typename T>
    T getGlobal(lua_State* L, const char* global) {
        lua_getglobal(L, global);
        T ret = check<T>(L, -1);

        lua_pop(L, 1);
        return ret;
    }

    template <typename T>
    T getTableGlobal(lua_State* L, const char* table, const char* key) {
        lua_getglobal(L, table);
        luaL_checktype(L, -1, LUA_TTABLE);

        lua_getfield(L, -1, key);
        T ret = check<T>(L, -1);

        lua_pop(L, 2);
        return ret;
    }

    template <typename T>
    T getArrayGlobal(lua_State* L, const char* array, int index) {
        lua_getglobal(L, array);
        luaL_checktype(L, -1, LUA_TTABLE);

        lua_geti(L, -1, index);
        T ret = check<T>(L, -1);

        lua_pop(L, 2);
        return ret;
    }
}

namespace LuaFunction {
    int print(lua_State* L);
    int exit(lua_State* L);
}

namespace LuaNames::Values {
    static constexpr const char* print = "printRaw";
    static constexpr const char* exit  = "exit";
}

#endif
