#ifndef HARP_ENTITY_TABLE_HPP
#define HARP_ENTITY_TABLE_HPP

#include <cstdlib>
#include <cstring>

#include <deque>
#include <exception>
#include <map>
#include <type_traits>
#include <vector>

#include "DynamicPoolAllocator.hpp"

template <typename T, typename = void>
struct canAdd : std::false_type {};
template <typename T>
struct canAdd<T, decltype(std::declval<T>() + std::declval<T>(), void())> : std::true_type {};
template <typename T>
static constexpr bool canAdd_v = canAdd<T>::value;

typedef unsigned int Entity;

class EntityTable;

template <typename T>
class LocalComponent {
    private:
        template <typename U>
        friend class LocalComponent;
        friend class EntityTable;

        unsigned int id;
        void (*op)(void);

    public:
        operator LocalComponent<void>() {
            LocalComponent<void> ret;
            ret.id = id;
            return ret;
        }
};

class ComponentBase {
    protected:
        static unsigned int nextId;
};

template <typename T>
class Component : ComponentBase {
    private:
        template <typename U>
        friend class Component;
        friend class EntityTable;

        unsigned int id;
        void (*op)(void);

        template <typename U, typename = void>
        struct def {
            static U op(U a, U b) { return a; }
        };
        template <typename U>
        struct def<U, std::enable_if_t<canAdd_v<U>>> {
            static U op(U a, U b) { return a + b; }
        };
        template <typename U>
        struct def<U, std::enable_if_t<std::is_same_v<U, void>>> {
            static constexpr void (*op)(void) = 0;
        };

        Component(unsigned int id) {
            this->id = id;
            this->op = 0;
        }

    public:
        Component() {
            id = nextId;
            op = (void(*)(void)) def<T>::op;
            ++nextId;
        }

        template <typename U, typename = std::enable_if_t<std::is_same_v<T, U>>>
        Component(U (*op)(U, U)) : Component() {
            this->op  = (void(*)(void))op;
        }

        operator Component<void>() {
            return Component<void>(id);
        }
};

class EntityTable {
    private:
        struct Delta {
            Entity ent;
            unsigned int comp;
        };

        struct EntMeta {
            bool orphan;
            Entity parent;
        };

        struct CompMeta {
            std::size_t size;
        };

        struct ValueMeta {
            bool exists;
        };

        void** data;

        EntMeta*    entMetadata;
        CompMeta*   compMetadata;
        ValueMeta** valueMetadata;

        unsigned int maxEnts;
        unsigned int maxComps;

        unsigned int nextEnt;
        unsigned int nextComp;

        std::map<unsigned int, unsigned int> global2local;
        DynamicPoolAllocator deltaPool;
        std::deque<Delta*> deltaQueue;

    public:
        class Iterator;

        EntityTable();
        ~EntityTable();

        Entity create();

        template <typename T>
        void use(Component<T> comp);
        template <typename T>
        LocalComponent<T> local(Component<T> comp);

        bool& owns(Entity ent, LocalComponent<void> comp);
        bool& owns(Entity ent, Component<void> comp);

        bool has(Entity ent, LocalComponent<void> comp);
        bool has(Entity ent, Component<void> comp);

        template <typename T>
        T& get(Entity ent, LocalComponent<T> comp);
        template <typename T>
        T& get(Entity ent, Component<T> comp);

        bool& get(Entity ent, LocalComponent<void> comp);
        bool& get(Entity ent, Component<void> comp);

        template <typename T>
        T view(Entity ent, LocalComponent<T> comp);
        template <typename T>
        T view(Entity ent, Component<T> comp);

        bool view(Entity ent, LocalComponent<void> comp);
        bool view(Entity ent, Component<void> comp);

        template <typename T>
        void set(Entity ent, LocalComponent<T> comp, T& value);
        template <typename T>
        void set(Entity ent, Component<T> comp, T& value);

        void set(Entity ent, LocalComponent<void> comp, bool value);
        void set(Entity ent, Component<void> comp, bool value);

        void apply();

        Entity& parent(Entity ent);
        bool& orphan(Entity ent);

        Iterator begin();
        Iterator end();

        template <typename... Ts>
        Iterator begin(LocalComponent<void> first, Ts... args);
        template <typename... Ts>
        Iterator begin(Component<void> first, Ts... args);

    private:
        void doubleMaxComps();
        void doubleMaxEnts();
};

class EntityTable::Iterator {
    private:
        friend class EntityTable;
        friend bool operator==(const Iterator&, const Iterator&);

        EntityTable* table;
        Entity currEnt;
        std::vector<LocalComponent<void>> required;

    public:
        Iterator() = delete;

        void require(LocalComponent<void> comp);

        Iterator& operator++();
        Entity operator*();

    private:
        Iterator(Entity ent, EntityTable* table);

        bool valid();
};

bool operator==(const EntityTable::Iterator& lhs, const EntityTable::Iterator& rhs);
bool operator!=(const EntityTable::Iterator& lhs, const EntityTable::Iterator& rhs);

//
// Template implementations
//

template <>
void EntityTable::use(Component<void> comp);

template <typename T>
void EntityTable::use(Component<T> comp) {
    if (global2local.count(comp.id) != 0) return;

    if (nextComp >= maxComps) doubleMaxComps();
    data[nextComp] = malloc(sizeof(T) * maxEnts);

    valueMetadata[nextComp] = (ValueMeta*) malloc(sizeof(ValueMeta) * maxEnts);
    compMetadata[nextComp].size = sizeof(T);

    memset(valueMetadata[nextComp], 0, sizeof(ValueMeta) * maxEnts);

    // This is an insert!
    global2local[comp.id] = nextComp;

    ++nextComp;
}

template <typename T>
LocalComponent<T> EntityTable::local(Component<T> comp) {
    if (global2local.count(comp.id) == 0) {
        throw std::invalid_argument("EntityTable::local: Table does not use this component");
    }

    LocalComponent<T> ret;
    ret.id  = global2local[comp.id];
    ret.op  = comp.op;
    return ret;
}

template <typename T>
T& EntityTable::get(Entity ent, LocalComponent<T> comp) {
    auto table = (T*) data[comp.id];
    owns(ent, comp) = true;
    return table[ent];
}

template <typename T>
T& EntityTable::get(Entity ent, Component<T> comp) {
    return get(ent, local(comp));
}

template <typename T>
T EntityTable::view(Entity ent, LocalComponent<T> comp) {
    static_assert(!std::is_same_v<T, void>);

    if (!has(ent, comp))
        throw new std::invalid_argument("EntityTable::view: Entity doesnt not have Component");

    T (*op)(T, T) = (T(*)(T,T))comp.op; // op is always valid if T isn't void

    if (orphan(ent))
        return get(ent, comp);
    if (!owns(ent, comp))
        return view(parent(ent), comp);
    if (has(parent(ent), comp))
        return op(view(parent(ent), comp), get(ent, comp));

    return get(ent, comp);
}

template <typename T>
T EntityTable::view(Entity ent, Component<T> comp) {
    return view(ent, local(comp));
}

template <typename T>
void EntityTable::set(Entity ent, LocalComponent<T> comp, T& value) {
    void* memory = deltaPool.alloc(sizeof(Delta) + sizeof(T));

    auto& delta = *(Delta*) memory;
    auto& data  = *(T*) (&delta + 1);

    delta.ent  = ent;
    delta.comp = comp.id;
    data       = value;

    deltaQueue.push_back(&delta);
}

template <typename T>
void EntityTable::set(Entity ent, Component<T> comp, T& value) {
    set(ent, local(comp), value);
}

template <typename... Ts>
EntityTable::Iterator EntityTable::begin(LocalComponent<void> first, Ts... args) {
    Iterator it = begin(args...);
    it.require(first);
    return it;
}

template <typename... Ts>
EntityTable::Iterator EntityTable::begin(Component<void> first, Ts... args) {
    return begin(local(first), args...);
}

#endif
