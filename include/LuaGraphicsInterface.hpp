#ifndef HARP_LUA_GRAPHICS_INTERFACE_HPP
#define HARP_LUA_GRAPHICS_INTERFACE_HPP

#include <type_traits>

extern "C" {
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
}

#include "graphics/Sprite.hpp"

class Color;
class FontRenderer;
class PrimitiveRenderer;
class Shader;
class Sprite;
struct VisualSpec;

namespace LuaGraphicsInterface {
    void load(lua_State* L);

    void addShader(lua_State* L, Shader& shd, const char* name);
    void addFontRenderer(lua_State* L, FontRenderer& fnt, const char* name);
    void addPrimitiveRenderer(lua_State* L, PrimitiveRenderer& prim, const char* name);

    void pushSprite(lua_State* L, Sprite& spr);
    void pushColor(lua_State* L, Color& col);
    void pushShader(lua_State* L, Shader& shd);
    void pushFontRenderer(lua_State* L, FontRenderer& fnt);
    void pushPrimitiveRenderer(lua_State* L, PrimitiveRenderer& prim);
    void pushVisualSpec(lua_State* L, VisualSpec& spec);

    Sprite& checkSprite(lua_State* L, int arg);
    Color checkColor(lua_State* L, int arg);
    Shader& checkShader(lua_State* L, int arg);
    FontRenderer& checkFontRenderer(lua_State* L, int arg);
    PrimitiveRenderer& checkPrimitiveRenderer(lua_State* L, int arg);
    VisualSpec checkVisualSpec(lua_State* L, int arg);
}

namespace LuaFunctions::nativesprite {
    int create(lua_State* L);
    int gc(lua_State* L);
}

namespace LuaHelper {
    template <typename T>
    inline typename std::enable_if_t<std::is_same_v<T, VisualSpec>, void>
    push(lua_State* L, T value) {
        LuaGraphicsInterface::pushVisualSpec(L, value);
    }

    template <typename T>
    inline typename std::enable_if_t<std::is_same_v<T, Color>, void>
    push(lua_State* L, T value) {
        LuaGraphicsInterface::pushColor(L, value);
    }

    template <typename T>
    inline typename std::enable_if_t<std::is_same_v<T, VisualSpec>, T>
    check(lua_State* L, int arg) {
        return LuaGraphicsInterface::checkVisualSpec(L, arg);
    }

    template <typename T>
    inline typename std::enable_if_t<std::is_same_v<T, Color>, T>
    check(lua_State* L, int arg) {
        return LuaGraphicsInterface::checkColor(L, arg);
    }
}

namespace LuaNames {
    namespace Types {
        static constexpr const char* nativesprite      = "harp.nativesprite";
        static constexpr const char* sprite            = "harp.sprite";
        static constexpr const char* shader            = "harp.shader";
        static constexpr const char* fontrenderer      = "harp.fontrenderer";
        static constexpr const char* primitiverenderer = "harp.primitiverenderer";
    }

    namespace Options {
        namespace nativesprite {
            static constexpr const char* path       = "path";
            static constexpr const char* x          = "x";
            static constexpr const char* y          = "y";
            static constexpr const char* width      = "width";
            static constexpr const char* height     = "height";
            static constexpr const char* texuniform = "texuniform";
            static constexpr const char* uvattrib   = "uvattrib";
        }

        namespace visualspec {
            static constexpr const char* drawtype = "drawtype";
            static constexpr const char* sprite   = "sprite";
            static constexpr const char* update   = "update";
            static constexpr const char* x        = "x";
            static constexpr const char* y        = "y";
            static constexpr const char* p1       = "p1";
            static constexpr const char* p2       = "p2";
            static constexpr const char* p3       = "p3";
            static constexpr const char* p4       = "p4";
            static constexpr const char* p5       = "p5";
            static constexpr const char* p6       = "p6";
            static constexpr const char* p7       = "p7";
            static constexpr const char* text     = "text";
            static constexpr const char* glyph    = "glyph";
            static constexpr const char* color    = "color";
            static constexpr const char* shader   = "shader";
            static constexpr const char* prim     = "prim";
            static constexpr const char* font     = "font";

            namespace drawtypeValue {
                static constexpr const char* sprite           = "sprite";
                static constexpr const char* rectangle        = "rectangle";
                static constexpr const char* roundedrectangle = "rounded rectangle";
                static constexpr const char* elipse           = "elipse";
                static constexpr const char* triangle         = "triangle";
                static constexpr const char* line             = "line";
                static constexpr const char* glyph            = "glyph";
                static constexpr const char* text             = "text";
            }
        }
    }

    namespace Values {
        static constexpr const char* nativespriteCreate = "NativeSprite";
        static constexpr const char* visualspecMeta     = "VisualSpec";

        static constexpr const char* shaderStore            = "shader";
        static constexpr const char* fontrendererStore      = "font";
        static constexpr const char* primitiverendererStore = "prim";
    }
}

#endif
