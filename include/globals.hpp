/*
 * HARP
 * globals.h
 *
 * This just defines some global variables, that's all
 *
 * - Callum Nicholson (hamsterjam)
 *
 */

#ifndef HARP_GLOBALS_HPP
#define HARP_GLOBALS_HPP

extern "C" {
#include <lua.h>
}

#include "Collider.hpp"
#include "EntityTable.hpp"
#include "graphics/VisualSpec.hpp"
#include "harpMath.hpp"

class Console;
class EntityTable;
class FontRenderer;
class PrimitiveRenderer;
class Shader;

void readConfig();
void initGlobals();
void cleanupGlobals();

extern lua_State* L;

extern EntityTable harp;

extern Component<Vec<2, double>> comp_position;
extern Component<Vec<2, double>> comp_velocity;
extern Component<Vec<2, double>> comp_acceleration;
extern Component<Collider>       comp_collider;
extern Component<VisualSpec>     comp_visual;
extern Component<int>            comp_layer;

extern Component<Vec<2, double>> comp_nextPosition;
extern Component<double>         comp_partialStep;
extern Component<Vec<2, double>> comp_onSurface;

extern Component<void> flag_hidden;
extern Component<void> flag_static;

extern Shader* defaultShader;
extern Shader* defaultPrimitiveShader;

extern PrimitiveRenderer* defaultPrim;
extern FontRenderer*      consoleFont;

extern int SCREEN_WIDTH;
extern int SCREEN_HEIGHT;

extern bool shouldExit;

#endif
