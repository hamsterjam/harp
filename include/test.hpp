#ifndef HARP_TEST_HPP
#define HARP_TEST_HPP

#include <iomanip>
#include <iostream>

#ifndef MESSAGE_LENGTH
#define MESSAGE_LENGTH 30
#endif

unsigned long TEST_firstfail;
unsigned long TEST_asserts;
unsigned long TEST_successes;

template <typename T>
void startTest(T message) {
    std::cout << std::left << std::setw(MESSAGE_LENGTH) << message << std::flush;
    TEST_asserts = 0;
    TEST_successes = 0;
    TEST_firstfail = 0;
}

bool testAssert(bool res) {
    TEST_asserts   += 1;
    TEST_successes += res ? 1 : 0;
    return !res && !TEST_firstfail;
}

#define testAssert(...) if (testAssert(__VA_ARGS__)) TEST_firstfail = __LINE__;

bool endTest() {
    double percentage = 100.0 * (double) TEST_successes / (double) TEST_asserts;
    std::cout << std::setprecision(3) << percentage << "%" << std::endl;
    return TEST_successes != TEST_asserts;
}

#define endTest(...) if (endTest(__VA_ARGS__)) \
    std::cerr << __FILE__ << ":" << TEST_firstfail << ": " \
              << "First failing assert here" << std::endl;

#endif
