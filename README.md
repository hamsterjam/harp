HARP
====

What is this?
-------------

This is a project I started a long time ago in order to learn about game development concepts.
It is far from complete, and far from being in a state where it would be useful for a game developer,
but some of the features it has at the moment include:

 * 2D graphics rendering done using OpenGL directly
 * Entity management using a fairly pure component based system
 * A kinematics / dynamics loop for platformer style games
 * Predictive collisions to prevent clipping through surfaces
 * Lua based configuration and scripting
 * Drop-down debug console

The features currently being worked on are:

 * Restructuring the code to work as a library
 * Code clean up

The project I have in mind for this engine is a side-scrolling platformer style RTS.
For this to work, the engine will have to support all the normal bells and whistles expected of modern platformers,
but uniquely, also pathfinding which should prove to be a fun challenge.

Building
--------

For now, there is no easy way to compile this as a library,
it can be compiled as an application, featuring whatever I am working on, and run with

```
$ make release
$ ./harp
```

This requires [SDL2](https://www.libsdl.org/download-2.0.php), [Lua](http://www.lua.org/),
and [stb_image.h](https://github.com/nothings/stb/blob/master/stb_image.h) (along with OpenGL, glew and glu).
Make sure your compiler / linker can find them!
