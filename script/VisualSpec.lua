require "script/Sprite"

VisualSpec = {}
VisualSpec.__index = VisualSpec
setmetatable(VisualSpec, {
    __call = function(cls, ...)
        local self = setmetatable({}, cls)
        self:_init(...)
        return self
    end
})

function VisualSpec:_init(drawtype)
    self.drawtype = drawtype
end

function VisualSpec.sprite(sprite, x, y, shd)
    local self = VisualSpec("sprite")
    self.sprite = NativeSprite(sprite)
    self.x      = x
    self.y      = y
    self.shader = shd

    return self
end

function VisualSpec.rectangle(x, y, width, height, linew, color, renderer)
    local self = VisualSpec("rectangle")
    self.p1    = x
    self.p2    = y
    self.p3    = width
    self.p4    = height
    self.p5    = linew
    self.color = color
    self.prim  = renderer
    return self
end

function VisualSpec.roundedrectangle(x, y, width, height, radius, linew, color, renderer)
    local self = VisualSpec("rounded rectangle")
    self.p1    = x
    self.p2    = y
    self.p3    = width
    self.p4    = height
    self.p5    = radius
    self.p6    = linew
    self.color = color
    self.prim  = renderer
    return self
end

function VisualSpec.elipsearc(x, y, radiusx, radiusy, theta1, theta2, linew, color, renderer)
    local self = VisualSpec("elipse")
    self.p1    = x
    self.p2    = y
    self.p3    = radiusx
    self.p4    = radiusy
    self.p5    = theta1
    self.p6    = theta2
    self.p7    = linew
    self.color = color
    self.prim  = renderer
end

function VisualSpec.triangle(x1, y1, x2, y2, x3, y3, linew, color, renderer)
    local self = VisualSpec("triangle")
    self.p1    = x1
    self.p2    = y1
    self.p3    = x2
    self.p4    = y2
    self.p5    = x3
    self.p6    = y3
    self.p7    = linew
    self.color = color
    self.prim  = renderer
    return self
end

function VisualSpec.line(x1, y1, x2, y2, linew, color, renderer)
    local self = VisualSpec("line")
    self.p1    = x1
    self.p2    = y1
    self.p3    = x2
    self.p4    = y2
    self.p5    = linew
    self.color = color
    self.prim  = renderer
    return self
end

function VisualSpec.glyph(glyph, x, y, renderer)
    local self = VisualSpec("glyph")
    self.x     = x
    self.y     = y
    self.glyph = glyph
    self.font  = renderer
    return self
end

function VisualSpec.text(text, x, y, renderer)
    local self = VisualSpec("text")
    self.x = x
    self.y = y
    self.text = text
    self.font = renderer
    return self
end

-- Rectangles
function VisualSpec.rectanglefill(x, y, width, height, color, renderer)
    return VisualSpec.rectangle(x, y, width, height, 0, color, renderer)
end
function VisualSpec.roundedrectanglefill(x, y, width, height, radius, color, renderer)
    return VisualSpec.roundedrectangle(x, y, width, height, raidus, 0, color, renderer)
end

-- Circles
function VisualSpec.circle(x, y, radius, linew, color, renderer)
    return VisualSpec.elipsearc(x, y, radius, radius, linew, color, renderer)
end
function VisualSpec.circlefill(x, y, radius, color, renderer)
    return VisualSpec.circle(x, y, radius, 0, color, renderer)
end
function VisualSpec.arc(x, y, radius, theta1, theta2, linew, color, renderer)
    return VisualSpec.elipsearc(x, y, radius, radius, theta1, theta2, linew, color, renderer)
end
function VisualSpec.segment(x, y, radius, theta1, theta2, color, renderer)
    return VisualSpec.arc(x, y, radius, theta1, theta2, 0, color, renderer)
end

-- Elipses
function VisualSpec.elipse(x, y, radiusx, radiusy, linew, color, renderer)
    return VisualSpec.elipsearc(x, y, radiusx, radiusy, 0, 360, linew, color, renderer)
end
function VisualSpec.elipsefill(x, y, radiusx, radiusy, color, renderer)
    return VisualSpec.elipse(x, y, radiusx, radiusy, 0, color, renderer)
end
function VisualSpec.elipsesegment(x, y, radiusx, radiusy, theta1, theta2, color, renderer)
    return VisualSpec.elipsearc(x, y, radiusx, radiusy, theta1, theta2, 0, color, renderer)
end

-- Triangles
function VisualSpec.trianglefill(x1, y1, x2, y2, x3, y3, color, renderer)
    return VisualSpec.trangle(x1, y1, x2, y2, x3, y3, 0, color, renderer)
end
