--
-- Sprite
--

Sprite = {}
Sprite.__index = Sprite
setmetatable(Sprite, {
    __call = function(cls, ...)
        local self = setmetatable({}, cls)
        self:_init(...)
        return self
    end,
})

--[[
    Valid options:
        path       - file path of texture
        x          - x coordinate of subtexture
        y          - y coordinate of subtexture
        width      - width of subtexture
        height     - height of subtexture
        texuniform - name of uniform to bind texture
        uvattrib   - name of attribute to bind uv coordinates to
]]--
function Sprite:_init(opt)
    -- If we just passed a single filename
    if type(opt) == "string" then
        opt = { path = opt }
    end

    -- If we passed a single option bag, wrap it in an array
    if opt.path ~= nil then
        opt = { opt }
    end

    -- Make sure x and y have good values
    for _,item in ipairs(opt) do
        if item.x == nil then item.x = 0 end
        if item.y == nil then item.y = 0 end
    end

    -- Just keep the data as is
    self.data = opt
end

function Sprite:add(opt)
    -- If we just passed a single filename
    if type(opt) == "string" then
        opt = { path = opt }
    end

    -- Make sure x and y have good values
    if opt.x == nil then opt.x = 0 end
    if opt.y == nil then opt.y = 0 end

    table.insert(self.data, opt)
    return self
end

function Sprite._test()
    -- Some simple sanity tests is all
    local sprite1 = Sprite("sprite1.png")
    assert(#sprite1.data == 1)
    assert(sprite1.data[1].path == "sprite1.png")

    local sprite2 = Sprite {path = "sprite2.png"}
    assert(#sprite2.data == 1)
    assert(sprite2.data[1].path == "sprite2.png")

    sprite2:add("sprite2-2.png")
    assert(#sprite2.data == 2)
    assert(sprite2.data[2].path == "sprite2-2.png")

    local sprite3 = Sprite {
        { path = "sprite3-1.png" },
        { path = "sprite3-2.png" }
    }

    assert(#sprite3.data == 2)
    assert(sprite3.data[1].path == "sprite3-1.png")
    assert(sprite3.data[2].path == "sprite3-2.png")
end

--
-- SpriteAtlas
--

SpriteAtlas = {}
SpriteAtlas.__index = SpriteAtlas
setmetatable(SpriteAtlas, {
    __call = function(cls, ...)
        local self = setmetatable({}, cls)
        self:_init(...)
        return self
    end,
})

function SpriteAtlas:_init(atlas, tilewidth, tileheight, padding, border)
    if padding == nil then padding = 0 end
    if border  == nil then border  = 0 end

    self.atlas      = atlas
    self.tilewidth  = tilewidth
    self.tileheight = tileheight
    self.padding    = padding
    self.border     = border
end

function SpriteAtlas:tile(tilex, tiley)
    local spriteoptions = {}
    for _,item in ipairs(self.atlas.data) do
        table.insert(spriteoptions, {
            path       = item.path,
            x          = item.x + self.border + (self.padding + self.tilewidth)  * (tilex - 1),
            y          = item.y + self.border + (self.padding + self.tileheight) * (tiley - 1),
            width      = self.tilewidth,
            height     = self.tileheight,
            texuniform = item.texuniform,
            uvattrib   = item.uvattrib,
        })
    end

    return Sprite(spriteoptions)
end

function SpriteAtlas._test()
    -- Some simple sanity tests is all
    local sprite = Sprite {
        {
            path = "sprite1.png",
            x    = 10,
            y    = 10,
        }, {
            path = "sprite2.png",
            x    = 10,
            y    = 10,
        }
    }

    local atlas = SpriteAtlas(sprite, 16, 16, 2, 10)

    local tile1 = atlas:tile(1, 1)
    assert(#tile1.data == 2)
    assert(tile1.data[1].path  == "sprite1.png")
    assert(tile1.data[2].path  == "sprite2.png")
    assert(tile1.data[1].x     == 20)
    assert(tile1.data[1].y     == 20)
    assert(tile1.data[1].width == 16)
    assert(tile1.data[1].width == 16)

    local tile2 = atlas:tile(3, 2)
    assert(tile2.data[1].x     == 56)
    assert(tile2.data[1].y     == 38)
    assert(tile2.data[1].width == 16)
    assert(tile2.data[1].width == 16)
end
