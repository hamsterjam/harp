Collider = {}
Collider.__index = Collider
setmetatable(Collider, {
    __call = function(cls, ...)
        local self = setmetatable({}, cls)
        self:_init(...)
        return self
    end,
})

function Collider:_init(colltype)
    self.colltype = colltype
end

function Collider.box(delta)
    local self = Collider("box")
    self.delta = delta
    return self
end

function Collider.line(delta)
    local self = Collider("line")
    self.delta = delta
    return self
end
