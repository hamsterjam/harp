require "script/harp"
require "script/Sprite"
require "script/VisualSpec"
require "script/Collider"

readFile = function(src)
    io.input(src)
    return io.read("a")
end

screenWidth  = 640
screenHeight = 480

defaultShader = {
    vertSrc = readFile("shader/default.vert"),
    fragSrc = readFile("shader/default.frag")
}

primShader = {
    vertSrc = readFile("shader/defaultPrim.vert"),
    fragSrc = readFile("shader/defaultPrim.frag")
}

init = (function()
    local ecs = table.harp
    local vis

    return function()
        local ecs = table.harp
        vis = VisualSpec.sprite(Sprite("res/test.png"), 0, 0, shader.def)

        miku = ecs:create()
        ecs:get(miku, comp.position, {200, 200})
        ecs:get(miku, comp.velocity, {0, 0})
        ecs:get(miku, comp.acceleration, {0, 0})
        ecs:get(miku, comp.collider, Collider.box{32, 32})
        ecs:get(miku, comp.visual, vis)

        floor = ecs:create()
        ecs:get(floor, comp.static, true)
        ecs:get(floor, comp.position, {0, 0})
        ecs:get(floor, comp.collider, Collider.line{screenWidth, 0})
    end
end)()
