#include <cstring>

extern "C" {
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
}

#include "LuaGraphicsInterface.hpp"
#include "graphics/Color.hpp"
#include "graphics/FontRenderer.hpp"
#include "graphics/PrimitiveRenderer.hpp"
#include "graphics/Shader.hpp"
#include "graphics/Sprite.hpp"
#include "graphics/VisualSpec.hpp"

using namespace LuaGraphicsInterface;

void LuaGraphicsInterface::load(lua_State* L) {
    // Metatable for harp.nativesprite
    luaL_newmetatable(L, LuaNames::Types::nativesprite);
    lua_pushcfunction(L, LuaFunctions::nativesprite::gc);
    lua_setfield(L, -2, "__gc");
    lua_pop(L, 1);

    lua_pushcfunction(L, LuaFunctions::nativesprite::create);
    lua_setglobal(L, LuaNames::Values::nativespriteCreate);

    // Metatables for other types
    luaL_newmetatable(L, LuaNames::Types::sprite);
    luaL_newmetatable(L, LuaNames::Types::shader);
    luaL_newmetatable(L, LuaNames::Types::fontrenderer);
    luaL_newmetatable(L, LuaNames::Types::primitiverenderer);
    lua_pop(L, 4);

    // Global Shader table
    lua_newtable(L);
    lua_setglobal(L, LuaNames::Values::shaderStore);

    // Global FontRenderer table
    lua_newtable(L);
    lua_setglobal(L, LuaNames::Values::fontrendererStore);

    // Global PrimitiveRenderer table
    lua_newtable(L);
    lua_setglobal(L, LuaNames::Values::primitiverendererStore);
}

//
// Add
//

void LuaGraphicsInterface::addShader(lua_State* L, Shader& shd, const char* name) {
    lua_getglobal(L, LuaNames::Values::shaderStore);
    LuaGraphicsInterface::pushShader(L, shd);
    lua_setfield(L, -2, name);
    lua_pop(L, 1);
}

void LuaGraphicsInterface::addFontRenderer(lua_State* L, FontRenderer& fnt, const char* name) {
    lua_getglobal(L, LuaNames::Values::fontrendererStore);
    LuaGraphicsInterface::pushFontRenderer(L, fnt);
    lua_setfield(L, -2, name);
    lua_pop(L, 1);
}

void LuaGraphicsInterface::addPrimitiveRenderer(lua_State* L, PrimitiveRenderer& prim, const char* name) {
    lua_getglobal(L, LuaNames::Values::primitiverendererStore);
    LuaGraphicsInterface::pushPrimitiveRenderer(L, prim);
    lua_setfield(L, -2, name);
    lua_pop(L, 1);
}

//
// Push
//

void LuaGraphicsInterface::pushSprite(lua_State* L, Sprite& spr) {
    auto& ret = *(Sprite**) lua_newuserdata(L, sizeof(Sprite*));
    luaL_setmetatable(L, LuaNames::Types::sprite);
    ret = &spr;
}

void LuaGraphicsInterface::pushColor(lua_State* L, Color& col) {
    lua_newtable(L);

    lua_pushnumber(L, col.r);
    lua_setfield(L, -2, "r");
    lua_pushnumber(L, col.g);
    lua_setfield(L, -2, "g");
    lua_pushnumber(L, col.b);
    lua_setfield(L, -2, "b");
    lua_pushnumber(L, col.a);
    lua_setfield(L, -2, "a");
}

void LuaGraphicsInterface::pushShader(lua_State* L, Shader& shd) {
    auto& ret = *(Shader**) lua_newuserdata(L, sizeof(Shader*));
    luaL_setmetatable(L, LuaNames::Types::shader);
    ret = &shd;
}

void LuaGraphicsInterface::pushFontRenderer(lua_State* L, FontRenderer& fnt) {
    auto& ret = *(FontRenderer**) lua_newuserdata(L, sizeof(FontRenderer*));
    luaL_setmetatable(L, LuaNames::Types::fontrenderer);
    ret = &fnt;
}

void LuaGraphicsInterface::pushPrimitiveRenderer(lua_State* L, PrimitiveRenderer& prim) {
    auto& ret = *(PrimitiveRenderer**) lua_newuserdata(L, sizeof(PrimitiveRenderer*));
    luaL_setmetatable(L, LuaNames::Types::primitiverenderer);
    ret = &prim;
}

void LuaGraphicsInterface::pushVisualSpec(lua_State* L, VisualSpec& spec) {
    lua_newtable(L);
    lua_getglobal(L, LuaNames::Values::visualspecMeta);
    lua_setmetatable(L, -2);

    const char* type;
    switch (spec.type) {
        case (DrawType::SPRITE):
            type = LuaNames::Options::visualspec::drawtypeValue::sprite;
            break;
        case (DrawType::RECTANGLE):
            type = LuaNames::Options::visualspec::drawtypeValue::rectangle;
            break;
        case (DrawType::ROUNDED_RECTANGLE):
            type = LuaNames::Options::visualspec::drawtypeValue::roundedrectangle;
            break;
        case (DrawType::ELIPSE):
            type = LuaNames::Options::visualspec::drawtypeValue::elipse;
            break;
        case (DrawType::TRIANGLE):
            type = LuaNames::Options::visualspec::drawtypeValue::triangle;
            break;
        case (DrawType::LINE):
            type = LuaNames::Options::visualspec::drawtypeValue::line;
            break;
        case (DrawType::GLYPH):
            type = LuaNames::Options::visualspec::drawtypeValue::glyph;
            break;
        case (DrawType::TEXT):
            type = LuaNames::Options::visualspec::drawtypeValue::text;
            break;
    }
    lua_pushstring(L, type);
    lua_setfield(L, -2, LuaNames::Options::visualspec::drawtype);

    char buffer[2] = {0};

    switch (spec.type) {
        case (DrawType::SPRITE):
            LuaGraphicsInterface::pushSprite(L, *spec.sprite.spr);
            lua_setfield(L, -2, LuaNames::Options::visualspec::sprite);

            lua_pushnumber(L, spec.sprite.x);
            lua_setfield(L, -2, LuaNames::Options::visualspec::x);

            lua_pushnumber(L, spec.sprite.y);
            lua_setfield(L, -2, LuaNames::Options::visualspec::y);

            LuaGraphicsInterface::pushShader(L, *spec.sprite.shd);
            lua_setfield(L, -2, LuaNames::Options::visualspec::shader);
            break;

        case (DrawType::RECTANGLE):
        case (DrawType::ROUNDED_RECTANGLE):
        case (DrawType::ELIPSE):
        case (DrawType::TRIANGLE):
        case (DrawType::LINE):
            LuaGraphicsInterface::pushPrimitiveRenderer(L, *spec.prim.prim);
            lua_setfield(L, -2, LuaNames::Options::visualspec::prim);

            lua_pushnumber(L, spec.prim.p1);
            lua_setfield(L, -2, LuaNames::Options::visualspec::p1);
            lua_pushnumber(L, spec.prim.p2);
            lua_setfield(L, -2, LuaNames::Options::visualspec::p2);
            lua_pushnumber(L, spec.prim.p3);
            lua_setfield(L, -2, LuaNames::Options::visualspec::p3);
            lua_pushnumber(L, spec.prim.p4);
            lua_setfield(L, -2, LuaNames::Options::visualspec::p4);
            lua_pushnumber(L, spec.prim.p5);
            lua_setfield(L, -2, LuaNames::Options::visualspec::p5);
            lua_pushnumber(L, spec.prim.p6);
            lua_setfield(L, -2, LuaNames::Options::visualspec::p6);
            lua_pushnumber(L, spec.prim.p7);
            lua_setfield(L, -2, LuaNames::Options::visualspec::p7);

            LuaGraphicsInterface::pushColor(L, spec.prim.color);
            lua_setfield(L, -2, LuaNames::Options::visualspec::color);
            break;

        case (DrawType::GLYPH):
        case (DrawType::TEXT):
            lua_pushnumber(L, spec.text.x);
            lua_setfield(L, -2, LuaNames::Options::visualspec::x);

            lua_pushnumber(L, spec.text.y);
            lua_setfield(L, -2, LuaNames::Options::visualspec::y);

            buffer[0] = spec.text.glyph;
            lua_pushstring(L, buffer);
            lua_setfield(L, -2, LuaNames::Options::visualspec::glyph);

            lua_pushstring(L, spec.text.text);
            lua_setfield(L, -2, LuaNames::Options::visualspec::text);

            LuaGraphicsInterface::pushFontRenderer(L, *spec.text.font);
            lua_setfield(L, -2, LuaNames::Options::visualspec::font);
            break;
    }
}

//
// Check
//

Sprite& LuaGraphicsInterface::checkSprite(lua_State* L, int arg) {
    bool isNative = luaL_testudata(L, arg, LuaNames::Types::nativesprite);
    if (isNative) return  *(Sprite* ) luaL_checkudata(L, arg, LuaNames::Types::nativesprite);
    else          return **(Sprite**) luaL_checkudata(L, arg, LuaNames::Types::sprite);
}

Color LuaGraphicsInterface::checkColor(lua_State* L, int arg) {
    Color ret;
    luaL_checktype(L, arg, LUA_TTABLE);

    lua_getfield(L, arg, "r");
    ret.r = luaL_checknumber(L, -1);
    lua_pop(L, 1);
    lua_getfield(L, arg, "g");
    ret.g = luaL_checknumber(L, -1);
    lua_pop(L, 1);
    lua_getfield(L, arg, "b");
    ret.b = luaL_checknumber(L, -1);
    lua_pop(L, 1);
    lua_getfield(L, arg, "a");
    ret.a = luaL_checknumber(L, -1);
    lua_pop(L, 1);

    return ret;
}

Shader& LuaGraphicsInterface::checkShader(lua_State* L, int arg) {
    return **(Shader**) luaL_checkudata(L, arg, LuaNames::Types::shader);
}

FontRenderer& LuaGraphicsInterface::checkFontRenderer(lua_State* L, int arg) {
    return **(FontRenderer**) luaL_checkudata(L, arg, LuaNames::Types::fontrenderer);
}

PrimitiveRenderer& LuaGraphicsInterface::checkPrimitiveRenderer(lua_State* L, int arg) {
    return **(PrimitiveRenderer**) luaL_checkudata(L, arg, LuaNames::Types::primitiverenderer);
}

VisualSpec LuaGraphicsInterface::checkVisualSpec(lua_State* L, int arg) {
    VisualSpec ret;

    luaL_checktype(L, arg, LUA_TTABLE);
    lua_getfield(L, arg, LuaNames::Options::visualspec::drawtype);
    const char* type = luaL_checkstring(L, -1);
    lua_pop(L, 1);

    if      (!strcmp(type, LuaNames::Options::visualspec::drawtypeValue::sprite))
        ret.type = DrawType::SPRITE;
    else if (!strcmp(type, LuaNames::Options::visualspec::drawtypeValue::rectangle))
        ret.type = DrawType::RECTANGLE;
    else if (!strcmp(type, LuaNames::Options::visualspec::drawtypeValue::roundedrectangle))
        ret.type = DrawType::ROUNDED_RECTANGLE;
    else if (!strcmp(type, LuaNames::Options::visualspec::drawtypeValue::elipse))
        ret.type = DrawType::ELIPSE;
    else if (!strcmp(type, LuaNames::Options::visualspec::drawtypeValue::triangle))
        ret.type = DrawType::TRIANGLE;
    else if (!strcmp(type, LuaNames::Options::visualspec::drawtypeValue::line))
        ret.type = DrawType::LINE;
    else if (!strcmp(type, LuaNames::Options::visualspec::drawtypeValue::glyph))
        ret.type = DrawType::GLYPH;
    else if (!strcmp(type, LuaNames::Options::visualspec::drawtypeValue::text))
        ret.type = DrawType::TEXT;

    const char* buff;

    switch (ret.type) {
        case (DrawType::SPRITE):
            lua_getfield(L, arg, LuaNames::Options::visualspec::shader);
            ret.sprite.shd = &LuaGraphicsInterface::checkShader(L, -1);
            lua_pop(L, 1);

            lua_getfield(L, arg, LuaNames::Options::visualspec::sprite);
            ret.sprite.spr = &LuaGraphicsInterface::checkSprite(L, -1);
            lua_pop(L, 1);

            lua_getfield(L, arg, LuaNames::Options::visualspec::x);
            ret.sprite.x = luaL_checknumber(L, -1);
            lua_pop(L,1);

            lua_getfield(L, arg, LuaNames::Options::visualspec::y);
            ret.sprite.y = luaL_checknumber(L, -1);
            lua_pop(L, 1);
            break;

        case (DrawType::RECTANGLE):
        case (DrawType::ROUNDED_RECTANGLE):
        case (DrawType::ELIPSE):
        case (DrawType::TRIANGLE):
        case (DrawType::LINE):
            lua_getfield(L, arg, LuaNames::Options::visualspec::prim);
            ret.prim.prim = &LuaGraphicsInterface::checkPrimitiveRenderer(L, -1);
            lua_pop(L, 1);

            lua_getfield(L, arg, LuaNames::Options::visualspec::p1);
            ret.prim.p1 = luaL_checknumber(L, -1); lua_pop(L, 1);
            lua_getfield(L, arg, LuaNames::Options::visualspec::p2);
            ret.prim.p2 = luaL_checknumber(L, -1); lua_pop(L, 1);
            lua_getfield(L, arg, LuaNames::Options::visualspec::p3);
            ret.prim.p3 = luaL_checknumber(L, -1); lua_pop(L, 1);
            lua_getfield(L, arg, LuaNames::Options::visualspec::p4);
            ret.prim.p4 = luaL_checknumber(L, -1); lua_pop(L, 1);
            lua_getfield(L, arg, LuaNames::Options::visualspec::p5);
            ret.prim.p5 = luaL_checknumber(L, -1); lua_pop(L, 1);

            lua_getfield(L, arg, LuaNames::Options::visualspec::p6);
            if (!lua_isnil(L, -1)) ret.prim.p6 = luaL_checknumber(L, -1);
            lua_pop(L, 1);

            lua_getfield(L, arg, LuaNames::Options::visualspec::p7);
            if (!lua_isnil(L, -1)) ret.prim.p7 = luaL_checknumber(L, -1);
            lua_pop(L, 1);
            break;

        case (DrawType::GLYPH):
        case (DrawType::TEXT):
            lua_getfield(L, arg, LuaNames::Options::visualspec::font);
            ret.text.font = &LuaGraphicsInterface::checkFontRenderer(L, -1);
            lua_pop(L, 1);

            lua_getfield(L, arg, LuaNames::Options::visualspec::glyph);
            if (!lua_isnil(L, -1)) {
                buff = luaL_checkstring(L, -1);
                ret.text.glyph = buff[0];
            }
            lua_pop(L, -1);

            lua_getfield(L, arg, LuaNames::Options::visualspec::text);
            if (!lua_isnil(L, -1)) ret.text.text = luaL_checkstring(L, -1);
            lua_pop(L, -1);

            lua_getfield(L, arg, LuaNames::Options::visualspec::x);
            ret.text.x = luaL_checknumber(L, -1);
            lua_pop(L, 1);

            lua_getfield(L, arg, LuaNames::Options::visualspec::y);
            ret.text.y = luaL_checknumber(L, -1);
            lua_pop(L, 1);
            break;
    }

    return ret;
}

//
// Naitve Sprite
//

int LuaFunctions::nativesprite::create(lua_State* L) {
    luaL_checktype(L, 1, LUA_TTABLE);

    auto ret = (Sprite*) lua_newuserdata(L, sizeof(Sprite));
    luaL_setmetatable(L, LuaNames::Types::nativesprite);
    new (ret) Sprite();

    lua_getfield(L, 1, "data");

    size_t numTex = luaL_len(L, -1);
    for (int j = 1; j <= numTex; ++j) {
        lua_geti(L, -1, j);
        luaL_checktype(L, -1, LUA_TTABLE);

        // path
        lua_getfield(L, -1, LuaNames::Options::nativesprite::path);
        const char* path = luaL_checkstring(L, -1);
        lua_pop(L, 1);

        // x
        lua_getfield(L, -1, LuaNames::Options::nativesprite::x);
        unsigned int x = luaL_checkinteger(L, -1);
        lua_pop(L, 1);

        // y
        lua_getfield(L, -1, LuaNames::Options::nativesprite::y);
        unsigned int y = luaL_checkinteger(L, -1);
        lua_pop(L, 1);

        // width
        lua_getfield(L, -1, LuaNames::Options::nativesprite::width);
        int width = 0;
        if (!lua_isnil(L, -1)) width = luaL_checkinteger(L, -1);
        lua_pop(L, 1);

        // height
        lua_getfield(L, -1, LuaNames::Options::nativesprite::height);
        int height = 0;
        if (!lua_isnil(L, -1)) height = luaL_checkinteger(L, -1);
        lua_pop(L, 1);

        // texuniform
        lua_getfield(L, -1, LuaNames::Options::nativesprite::texuniform);
        const char* texuniform = "uTexture"; // TODO // Magic string
        if (!lua_isnil(L, -1)) texuniform = luaL_checkstring(L, -1);
        lua_pop(L, 1);

        // uvattrib
        lua_getfield(L, -1, LuaNames::Options::nativesprite::uvattrib);
        const char* uvattrib = "aTexCoord"; // TODO // Magic string
        if (!lua_isnil(L, -1)) uvattrib = luaL_checkstring(L, -1);
        lua_pop(L, 2);

        if (width == 0 && height == 0)
            ret->addImage(path, texuniform, uvattrib);
        else
            ret->addSubImage(path, texuniform, uvattrib, x, y, width, height);

    }
    lua_pop(L, 1);

    return 1;
}

int LuaFunctions::nativesprite::gc(lua_State* L) {
    auto& value = *(Sprite*) luaL_checkudata(L, 1, LuaNames::Types::nativesprite);
    value.~Sprite();

    return 0;
}
