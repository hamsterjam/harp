#include <string>

extern "C" {
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
}

#include "Console.hpp"
#include "LuaEntityInterface.hpp"
#include "LuaGraphicsInterface.hpp"
#include "LuaHelper.hpp"
#include "globals.hpp"

static constexpr const char* WEAK_TABLE_KEY = "harp.weaktable";

void LuaHelper::loadHarp(lua_State* L) {
    LuaHelper::load(L);
    LuaEntityInterface::load(L);
    LuaGraphicsInterface::load(L);
}

void LuaHelper::load(lua_State* L) {
    // Init weak reference table
    lua_newtable(L);
    lua_newtable(L);
    lua_pushstring(L, "v");
    lua_setfield(L, -2, "__mode");
    lua_setmetatable(L, -2);
    lua_setfield(L, LUA_REGISTRYINDEX, WEAK_TABLE_KEY);

    lua_pushcfunction(L, LuaFunction::print);
    lua_setglobal(L, LuaNames::Values::print);

    lua_pushcfunction(L, LuaFunction::exit);
    lua_setglobal(L, LuaNames::Values::exit);
}

int LuaHelper::weakRef(lua_State* L) {
    lua_getfield(L, LUA_REGISTRYINDEX, WEAK_TABLE_KEY);
    lua_insert(L, -2);

    int ret = luaL_ref(L, -2);
    lua_pop(L, 1);
    return ret;
}

void LuaHelper::pushWeakRef(lua_State* L, int ref) {
    lua_getfield(L, LUA_REGISTRYINDEX, WEAK_TABLE_KEY);
    lua_geti(L, -1, ref);
    lua_remove(L, -2);
}

int LuaFunction::print(lua_State* L) {
    const char* message = luaL_checkstring(L, 1);
    Console::getInstance().log(std::string("<- ") + message);
    return 0;
}

int LuaFunction::exit(lua_State* L) {
    shouldExit = true;
    return 0;
}

