extern "C" {
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
}

#include <cstdlib>
#include <iostream>

#include "Collider.hpp"
#include "Console.hpp"
#include "EntityTable.hpp"
#include "FunctionWrapper.hpp"
#include "LuaGraphicsInterface.hpp" // Needed before LuaEntityInterface.hpp
#include "LuaEntityInterface.hpp"
#include "LuaHelper.hpp"
#include "globals.hpp"
#include "graphics/FontRenderer.hpp"
#include "graphics/PrimitiveRenderer.hpp"
#include "graphics/Shader.hpp"
#include "graphics/Sprite.hpp"
#include "graphics/TextureAtlas.hpp"
#include "graphics/VisualSpec.hpp"
#include "harpMath.hpp"

static const char* defVertSrc = "";
static const char* defFragSrc = "";

static const char* primVertSrc = "";
static const char* primFragSrc = "";

using namespace LuaHelper;

void readConfig() {
    luaL_openlibs(L);

    if (luaL_loadfile(L, "config.lua") || lua_pcall(L, 0, 0, 0)) {
        std::cerr << "Failed to load config.lua" << std::endl;
        std::cerr << lua_tostring(L, -1) << std::endl;
        exit(1);
    }

    SCREEN_WIDTH  = getGlobal<int>(L, "screenWidth");
    SCREEN_HEIGHT = getGlobal<int>(L, "screenHeight");

    defVertSrc = getTableGlobal<const char*>(L, "defaultShader", "vertSrc");
    defFragSrc = getTableGlobal<const char*>(L, "defaultShader", "fragSrc");

    primVertSrc = getTableGlobal<const char*>(L, "primShader", "vertSrc");
    primFragSrc = getTableGlobal<const char*>(L, "primShader", "fragSrc");

    // Adding in the harp functions after loading the config ensures that the
    // config file can't do anything to the state of the actual program
    loadHarp(L);
}

void initGlobals() {
    defaultShader = new Shader(defVertSrc, defFragSrc);
    defaultPrimitiveShader = new Shader(primVertSrc, primFragSrc);

    harp.use(comp_position);
    harp.use(comp_velocity);
    harp.use(comp_acceleration);
    harp.use(comp_collider);
    harp.use(comp_visual);
    harp.use(comp_layer);

    harp.use(comp_nextPosition);
    harp.use(comp_partialStep);
    harp.use(comp_onSurface);

    harp.use(flag_hidden);
    harp.use(flag_static);

    LuaEntityInterface::addTable(L, harp, "harp");

    LuaEntityInterface::addComponent(L, comp_position,     "position");
    LuaEntityInterface::addComponent(L, comp_velocity,     "velocity");
    LuaEntityInterface::addComponent(L, comp_acceleration, "acceleration");
    LuaEntityInterface::addComponent(L, comp_collider,     "collider");
    LuaEntityInterface::addComponent(L, comp_visual,       "visual");
    LuaEntityInterface::addComponent(L, comp_layer,        "layer");

    LuaEntityInterface::addComponent(L, comp_nextPosition, "nextPosition");
    LuaEntityInterface::addComponent(L, comp_partialStep,  "partialStep");
    LuaEntityInterface::addComponent(L, comp_onSurface,    "onSurface");

    LuaEntityInterface::addComponent(L, flag_hidden,       "hidden");
    LuaEntityInterface::addComponent(L, flag_static,       "static");

    // Console
    defaultPrim = new PrimitiveRenderer();
    TextureAtlas consoleFontAtlas("res/testfont.png", 8, 12, 0, 0);
    consoleFont = new FontRenderer(consoleFontAtlas, ' ', '~');

    LuaGraphicsInterface::addShader(L, *defaultShader, "def");
    LuaGraphicsInterface::addFontRenderer(L, *consoleFont, "console");
    LuaGraphicsInterface::addPrimitiveRenderer(L, *defaultPrim, "def");

    Console::init(*defaultPrim, *consoleFont);

    // Call the Lua init function
    lua_getglobal(L, "init");
    if (lua_pcall(L, 0, 0, 0)) {
        const char* errorMessage = lua_tostring(L, -1);
        std::cerr << errorMessage << std::endl;
        lua_pop(L, 1);
    }
}

void cleanupGlobals() {
    Console::cleanup();

    lua_close(L);

    delete defaultPrim;
    delete consoleFont;

    delete defaultShader;
    delete defaultPrimitiveShader;
}

//
// Default Values
//

lua_State* L = luaL_newstate();

/* EntityTable harp(32, 8, 128); */
EntityTable harp;

Component<Vec<2, double>> comp_position;
Component<Vec<2, double>> comp_velocity;
Component<Vec<2, double>> comp_acceleration;
Component<Collider>       comp_collider;
Component<VisualSpec>     comp_visual;
Component<int>            comp_layer;

Component<Vec<2, double>> comp_nextPosition;
Component<double>         comp_partialStep;
Component<Vec<2, double>> comp_onSurface;

Component<void> flag_hidden;
Component<void> flag_static;


Shader* defaultShader = 0;
Shader* defaultPrimitiveShader = 0;

PrimitiveRenderer* defaultPrim = 0;
FontRenderer*      consoleFont = 0;

int SCREEN_WIDTH  = 0;
int SCREEN_HEIGHT = 0;

bool shouldExit = false;
