#include "EntityTable.hpp"
#include "globals.hpp"
#include "harpMath.hpp"
#include "systems.hpp"

const double omega = 0.01;

void system_fudge(EntityTable& ecs) {
    for (auto it = ecs.begin(comp_velocity); it != ecs.end(); ++it) {
        Entity e = *it;

        Vec<2, double> vel = ecs.get(e, comp_velocity);

        // Fudge velocity to be zero if it is very close to zero
        bool velChanged = false;
        for (int i = 0; i < 2; ++i) {
            double abs = (vel[i] < 0) ? -vel[i] : vel[i];
            if (abs < omega) {
                vel[i] = 0;
                velChanged = true;
            }
        }

        if (velChanged) ecs.set(e, comp_velocity, vel);
    }
}
