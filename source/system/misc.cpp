#include "EntityTable.hpp"
#include "globals.hpp"
#include "harpMath.hpp"
#include "systems.hpp"

void system_zeroAcceleration(EntityTable& ecs) {
    Vec<2, double> zero = zeroVec<2, double>();
    for (auto it = ecs.begin(comp_acceleration); it != ecs.end(); ++it) {
        Entity e = *it;
        ecs.set(e, comp_acceleration, zero);
    }
}
