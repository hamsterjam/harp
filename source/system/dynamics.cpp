#include "EntityTable.hpp"
#include "globals.hpp"
#include "harpMath.hpp"
#include "systems.hpp"

void system_dynamics(EntityTable& ecs) {
    for (auto it = ecs.begin(comp_acceleration); it != ecs.end(); ++it) {
        Entity e = *it;
        if (ecs.get(e, flag_static)) continue;

        // If no partialStep component, then this is the first loop and we need
        // to generate it
        if (!ecs.owns(e, comp_partialStep)) {
            double partialStep = 1;
            ecs.get(e, comp_partialStep) = partialStep;
        }
        // If it is 0 then we are done processing
        else if (ecs.get(e, comp_partialStep) == 0) continue;

        Vec<2, double> acc = ecs.get(e, comp_acceleration);

        if (ecs.owns(e, comp_onSurface)) {
            // On a surface

            // Friction
            if (ecs.owns(e, comp_velocity)) {
                auto vel = ecs.get(e, comp_velocity);
                acc -= 2 * vel;
            }
        }
        else {
            // Not on a surface

            // Gravity
            double gravData[] = {
                   0,
                -200
            };
            acc += Vec<2, double>(gravData);

            // Friction
            if (ecs.owns(e, comp_velocity)) {
                auto vel = ecs.get(e, comp_velocity);
                acc -= 0.1 * vel;
            }
        }


        ecs.set(e, comp_acceleration, acc);
    }
}
