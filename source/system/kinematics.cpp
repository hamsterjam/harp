#include "EntityTable.hpp"
#include "globals.hpp"
#include "harpMath.hpp"
#include "systems.hpp"

void system_proposeKinematics(EntityTable& ecs, unsigned int deltaT) {
    double sDeltaT = (double) deltaT / 1000.0;

    // Update the position based on velocity
    for (auto it = ecs.begin(comp_position, comp_velocity); it != ecs.end(); ++it) {
        Entity e = *it;

        if (ecs.get(e, flag_static)) continue;
        // Might still not have a partial step, (if it has no acceleration)
        double partialStep = 1;
        if (!ecs.owns(e, comp_partialStep)) {
            ecs.set(e, comp_partialStep, partialStep);
        }
        else {
            partialStep = ecs.get(e, comp_partialStep);
            if (partialStep == 0) continue;
        }

        double stepT = partialStep * sDeltaT;

        Vec<2, double> pos = ecs.get(e, comp_position);
        Vec<2, double> vel = ecs.get(e, comp_velocity);
        Vec<2, double> acc = zeroVec<2, double>();

        if (ecs.owns(e, comp_acceleration)) {
            acc = ecs.get(e, comp_acceleration);
        }

        pos += (stepT * vel) + (0.5 * stepT * stepT * acc);

        // Setting nextPosition so the collision system knows both where we
        // are going, and where we came from so we can do better collisions
        // than standard clipping collisions
        ecs.set(e, comp_nextPosition, pos);
    }

    // Update the velocity based on acceleration
    for (auto it = ecs.begin(comp_velocity, comp_acceleration); it != ecs.end(); ++it) {
        Entity e = *it;

        if (ecs.get(e, flag_static)) continue;
        if (ecs.owns(e, comp_partialStep) && ecs.get(e, comp_partialStep) == 0) continue;

        double stepT = sDeltaT;
        stepT *= ecs.get(e, comp_partialStep);

        Vec<2, double> vel = ecs.get(e, comp_velocity);
        Vec<2, double> acc = ecs.get(e, comp_acceleration);

        vel += stepT * acc;

        ecs.set(e, comp_velocity, vel);
    }
}

void system_resolveKinematics(EntityTable& ecs) {
    // If we are on a surface, remove all acc and vel perpindicular to that surface
    for (auto it = ecs.begin(comp_onSurface); it!= ecs.end(); ++it) {
        Entity e = *it;
        Vec<2, double> surfNorm = ecs.get(e, comp_onSurface);

        if (ecs.owns(e, comp_velocity)) {
            Vec<2, double> vel = ecs.get(e, comp_velocity);
            vel -= proj(vel, surfNorm);
            ecs.set(e, comp_velocity, vel);
        }
        if (ecs.owns(e, comp_acceleration)) {
            Vec<2, double> acc = ecs.get(e, comp_acceleration);
            acc -= proj(acc, surfNorm);
            ecs.set(e, comp_acceleration, acc);
        }
    }

    // Perform the partial step
    for (auto it = ecs.begin(comp_position, comp_nextPosition); it != ecs.end(); ++it) {
        Entity e = *it;
        Vec<2, double> newPos = ecs.get(e, comp_nextPosition);
        ecs.set(e, comp_position, newPos);
        ecs.owns(e, comp_nextPosition) = false;
    }
}
