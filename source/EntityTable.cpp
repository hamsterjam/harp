#include <cstdlib>
#include <cstring>
#include <utility>

#include "EntityTable.hpp"

unsigned int ComponentBase::nextId = 0;

EntityTable::EntityTable() : deltaPool(0x800) {
    maxEnts  = 1;
    maxComps = 1;

    data = (void**) malloc(sizeof(void*));

    entMetadata   = (EntMeta*)    malloc(sizeof(EntMeta));
    compMetadata  = (CompMeta*)   malloc(sizeof(CompMeta));
    valueMetadata = (ValueMeta**) malloc(sizeof(ValueMeta*));

    nextEnt  = 0;
    nextComp = 0;
}

EntityTable::~EntityTable() {
    for (int i = 0; i < nextComp; ++i) {
        free(data[i]);
        free(valueMetadata[i]);
    }
    free(data);

    free(entMetadata);
    free(compMetadata);
    free(valueMetadata);
}

Entity EntityTable::create() {
    if (nextEnt >= maxEnts) doubleMaxEnts();

    Entity ret = nextEnt;
    ++nextEnt;

    entMetadata[ret].orphan = true;

    return ret;
}

template<>
void EntityTable::use(Component<void> comp) {
    if (nextComp >= maxComps) doubleMaxComps();

    data[nextComp] = malloc(0);

    valueMetadata[nextComp] = (ValueMeta*) malloc(sizeof(ValueMeta) * maxEnts);
    compMetadata[nextComp].size = 0;

    memset(valueMetadata[nextComp], 0, sizeof(ValueMeta) * maxEnts);

    // This is an insert!
    global2local[comp.id] = nextComp;

    ++nextComp;
}


bool& EntityTable::owns(Entity ent, LocalComponent<void> comp) {
    return valueMetadata[comp.id][ent].exists;
}

bool& EntityTable::owns(Entity ent, Component<void> comp) {
    return owns(ent, local(comp));
}

bool EntityTable::has(Entity ent, LocalComponent<void> comp) {
    if (orphan(ent)) return owns(ent, comp);

    return owns(ent, comp) || has(entMetadata[ent].parent, comp);
}

bool EntityTable::has(Entity ent, Component<void> comp) {
    return has(ent, local(comp));
}

bool& EntityTable::get(Entity ent, LocalComponent<void> comp) {
    return valueMetadata[comp.id][ent].exists;
}

bool& EntityTable::get(Entity ent, Component<void> comp) {
    return get(ent, local(comp));
}

bool EntityTable::view(Entity ent, LocalComponent<void> comp) {
    if (get(ent, comp)) return true;
    if (!orphan(ent)) return view(parent(ent), comp);
    return false;
}

bool EntityTable::view(Entity ent, Component<void> comp) {
    return view(ent, local(comp));
}

void EntityTable::set(Entity ent, LocalComponent<void> comp, bool value) {
    void* memory = deltaPool.alloc(sizeof(Delta) + sizeof(bool));

    auto& delta = *(Delta*) memory;
    auto& data  = *(bool*) (&delta + 1);

    delta.ent  = ent;
    delta.comp = comp.id;
    data       = value;

    deltaQueue.push_back(&delta);
}

void EntityTable::set(Entity ent, Component<void> comp, bool value) {
    set(ent, local(comp), value);
}

void EntityTable::apply() {
    for (Delta* pDelta : deltaQueue) {
        auto& delta = *pDelta;
        void* value = (void*) (pDelta + 1);

        Entity       ent  = delta.ent;
        unsigned int comp = delta.comp;

        std::size_t size = compMetadata[comp].size;

        auto table = (char*) data[comp];
        auto entry = (char*) &table[ent * size];

        if (size != 0) {
            valueMetadata[comp][ent].exists = true;
            memcpy(entry, value, size);
        }
        else {
            valueMetadata[comp][ent].exists = *(bool*)value;
        }
    }

    deltaQueue.clear();
    deltaPool.freeAll();
}

Entity& EntityTable::parent(Entity ent) {
    orphan(ent) = false;
    return entMetadata[ent].parent;
}

bool& EntityTable::orphan(Entity ent)
{
    return entMetadata[ent].orphan;
}

void EntityTable::doubleMaxComps() {
    maxComps *= 2;
    data = (void**) realloc(data, sizeof(void*) * maxComps);

    compMetadata  = (CompMeta*)   realloc(compMetadata,  sizeof(CompMeta)   * maxComps);
    valueMetadata = (ValueMeta**) realloc(valueMetadata, sizeof(ValueMeta*) * maxComps);
}

void EntityTable::doubleMaxEnts() {
    maxEnts *= 2;
    for (int i = 0; i < nextComp; ++i) {
        data[i] = realloc(data[i], compMetadata[i].size * maxEnts);

        valueMetadata[i] = (ValueMeta*) realloc(valueMetadata[i], sizeof(ValueMeta) * maxEnts);
        memset(valueMetadata[i] + nextEnt, 0, sizeof(ValueMeta) * (maxEnts - nextEnt));
    }

    entMetadata = (EntMeta*) realloc(entMetadata, sizeof(EntMeta) * maxEnts);
}

EntityTable::Iterator EntityTable::begin() {
    return Iterator(0, this);
}

EntityTable::Iterator EntityTable::end() {
    return Iterator(nextEnt, this);
}

//
// EntityTable::Iterator
//

EntityTable::Iterator::Iterator(Entity ent, EntityTable* table) {
    this->currEnt = ent;
    this->table   = table;
}

EntityTable::Iterator& EntityTable::Iterator::operator++() {
    ++currEnt;
    while (!valid()) ++currEnt;
    return *this;
}

Entity EntityTable::Iterator::operator*() {
    return currEnt;
}

void EntityTable::Iterator::require(LocalComponent<void> comp) {
    required.push_back(comp);
    if (!valid()) ++(*this);
}

bool EntityTable::Iterator::valid() {
    if (*this == table->end()) return true;

    bool valid = true;
    for (auto comp : required) {
        if (!table->owns(currEnt, comp)) {
            valid = false;
            break;
        }
    }
    return valid;
}

bool operator==(const EntityTable::Iterator& lhs, const EntityTable::Iterator& rhs) {
    return lhs.currEnt == rhs.currEnt;
}

bool operator!=(const EntityTable::Iterator& lhs, const EntityTable::Iterator& rhs) {
    return !(lhs == rhs);
}
