extern "C" {
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
}

#include "EntityTable.hpp"
#include "LuaEntityInterface.hpp"

void LuaEntityInterface::load(lua_State* L) {
    luaL_newmetatable(L, LuaNames::Types::entitytable);
    luaL_newmetatable(L, LuaNames::Types::entity);
    luaL_newmetatable(L, LuaNames::Types::component);

    lua_pop(L, 3);

    { // Metatable for harp.entitytable
        luaL_getmetatable(L, LuaNames::Types::entitytable);

        lua_pushvalue(L, -1); // Set __index to self
        lua_setfield(L, -2, "__index");

        lua_pushcfunction(L, LuaFunction::entitytable::create); lua_setfield(L, -2, LuaNames::Values::entitytable::create);
        lua_pushcfunction(L, LuaFunction::entitytable::owns);   lua_setfield(L, -2, LuaNames::Values::entitytable::owns);
        lua_pushcfunction(L, LuaFunction::entitytable::has);    lua_setfield(L, -2, LuaNames::Values::entitytable::has);
        lua_pushcfunction(L, LuaFunction::entitytable::get);    lua_setfield(L, -2, LuaNames::Values::entitytable::get);
        lua_pushcfunction(L, LuaFunction::entitytable::view);   lua_setfield(L, -2, LuaNames::Values::entitytable::view);
        lua_pushcfunction(L, LuaFunction::entitytable::set);    lua_setfield(L, -2, LuaNames::Values::entitytable::set);
        lua_pushcfunction(L, LuaFunction::entitytable::apply);  lua_setfield(L, -2, LuaNames::Values::entitytable::apply);
        lua_pushcfunction(L, LuaFunction::entitytable::parent); lua_setfield(L, -2, LuaNames::Values::entitytable::parent);
        lua_pushcfunction(L, LuaFunction::entitytable::orphan); lua_setfield(L, -2, LuaNames::Values::entitytable::orphan);

        lua_pop(L, 1);
    }
    { // Global table for harp.entitytables
        lua_newtable(L);
        lua_setglobal(L, LuaNames::Values::entitytableStore);
    }
    { // Global table for harp.components
        lua_newtable(L);
        lua_setglobal(L, LuaNames::Values::componentStore);
    }
}

void LuaEntityInterface::addTable(lua_State* L, EntityTable& table, const char* name) {
    lua_getglobal(L, LuaNames::Values::entitytableStore);
    LuaEntityInterface::pushEntityTable(L, table);
    lua_setfield(L, -2, name);
    lua_pop(L, 1);
}

void LuaEntityInterface::pushEntityTable(lua_State* L, EntityTable& table) {
    auto ret = (EntityTable**) lua_newuserdata(L, sizeof(EntityTable*));
    luaL_setmetatable(L, LuaNames::Types::entitytable);
    *ret = &table;
}

void LuaEntityInterface::pushEntity(lua_State* L, Entity ent) {
    auto ret = (Entity*) lua_newuserdata(L, sizeof(Entity));
    luaL_setmetatable(L, LuaNames::Types::entity);
    *ret = ent;
}

EntityTable& LuaEntityInterface::checkEntityTable(lua_State* L, int arg) {
    return **(EntityTable**) luaL_checkudata(L, arg, LuaNames::Types::entitytable);
}

Entity LuaEntityInterface::checkEntity(lua_State* L, int arg) {
    return *(Entity*) luaL_checkudata(L, arg, LuaNames::Types::entity);
}

//
// Functions for harp.entitytable
//

int LuaFunction::entitytable::create(lua_State* L) {
    EntityTable& table = LuaEntityInterface::checkEntityTable(L, 1);
    Entity ent = table.create();
    LuaEntityInterface::pushEntity(L, ent);
    return 1;
}

int LuaFunction::entitytable::owns(lua_State* L) {
    int args = lua_gettop(L);
    luaL_checktype(L, 3, LUA_TTABLE);
    lua_getfield(L, 3, LuaNames::Values::component::owns);
    lua_insert(L, 1);
    lua_call(L, args, 1);
    return 1;
}

int LuaFunction::entitytable::has(lua_State* L) {
    int args = lua_gettop(L);
    luaL_checktype(L, 3, LUA_TTABLE);
    lua_getfield(L, 3, LuaNames::Values::component::has);
    lua_insert(L, 1);
    lua_call(L, args, 1);
    return 1;
}

int LuaFunction::entitytable::get(lua_State* L) {
    int args = lua_gettop(L);
    luaL_checktype(L, 3, LUA_TTABLE);
    lua_getfield(L, 3, LuaNames::Values::component::get);
    lua_insert(L, 1);
    lua_call(L, args, 1);
    return 1;
}

int LuaFunction::entitytable::view(lua_State* L) {
    int args = lua_gettop(L);
    luaL_checktype(L, 3, LUA_TTABLE);
    lua_getfield(L, 3, LuaNames::Values::component::view);
    lua_insert(L, 1);
    lua_call(L, args, 1);
    return 1;
}

int LuaFunction::entitytable::set(lua_State* L) {
    int args = lua_gettop(L);
    luaL_checktype(L, 3, LUA_TTABLE);
    lua_getfield(L, 3, LuaNames::Values::component::set);
    lua_insert(L, 1);
    lua_call(L, args, 0);
    return 0;
}

int LuaFunction::entitytable::apply(lua_State* L) {
    EntityTable& table = LuaEntityInterface::checkEntityTable(L, 1);
    table.apply();
    return 0;
}

int LuaFunction::entitytable::parent(lua_State* L) {
    EntityTable& table = LuaEntityInterface::checkEntityTable(L, 1);
    Entity       ent   = LuaEntityInterface::checkEntity(L,      2);

    if (lua_gettop(L) >= 3) {
        Entity value = LuaEntityInterface::checkEntity(L, 3);
        table.parent(ent) = value;
    }

    LuaEntityInterface::pushEntity(L, table.parent(ent));
    return 1;
}

int LuaFunction::entitytable::orphan(lua_State* L) {
    EntityTable& table = LuaEntityInterface::checkEntityTable(L, 1);
    Entity       ent   = LuaEntityInterface::checkEntity(L,      2);

    if (lua_gettop(L) >= 3) {
        bool value = lua_toboolean(L, 3);
        table.orphan(ent) = value;
    }

    lua_pushboolean(L, table.orphan(ent));
    return 1;
}

//
// Functions for harp.component
//

template <>
int LuaFunction::component::get<void>(lua_State* L) {
    EntityTable&    table = LuaEntityInterface::checkEntityTable(L,     1);
    Entity          ent   = LuaEntityInterface::checkEntity(L,          2);
    Component<void> comp  = LuaEntityInterface::checkComponent<void>(L, 3);

    if (lua_gettop(L) >= 4) {
        bool value = lua_toboolean(L, 4);
        table.get(ent, comp) = value;
    }

    lua_pushboolean(L, table.get(ent, comp));
    return 1;
}

template <>
int LuaFunction::component::view<void>(lua_State* L) {
    EntityTable&    table = LuaEntityInterface::checkEntityTable(L,     1);
    Entity          ent   = LuaEntityInterface::checkEntity(L,          2);
    Component<void> comp  = LuaEntityInterface::checkComponent<void>(L, 3);

    lua_pushboolean(L, table.view(ent, comp));
    return 1;
}

template <>
int LuaFunction::component::set<void>(lua_State* L) {
    EntityTable&    table = LuaEntityInterface::checkEntityTable(L,     1);
    Entity          ent   = LuaEntityInterface::checkEntity(L,          2);
    Component<void> comp  = LuaEntityInterface::checkComponent<void>(L, 3);
    bool            value = lua_toboolean(L,                            4);

    table.set(ent, comp, value);
    return 0;
}
