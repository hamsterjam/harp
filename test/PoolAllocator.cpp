#include "test.hpp"

#include "PoolAllocator.hpp"

int main(int argc, char** argv) {
    {
        startTest("Simple allocations...");

        PoolAllocator pool(100 * sizeof(int));

        int* pointers[100];
        for (int i = 0; i < 100; ++i) {
            pointers[i]  = (int*) pool.alloc(sizeof(int));
            *pointers[i] = i;
        }
        for (int i = 0; i < 100; ++i) {
            testAssert(*pointers[i] == i);
        }

        pool.freeAll();

        endTest();
    }
    {
        startTest("Failed allocations...");

        PoolAllocator pool(1);

        testAssert(pool.alloc(1));
        testAssert(!pool.alloc(1));
        pool.freeAll();
        testAssert(pool.alloc(1));

        pool.freeAll();

        endTest();
    }
    {
        startTest("Contiguous allocations...");

        PoolAllocator pool(100 * sizeof(int));

        int* first = (int*) pool.alloc(sizeof(int));
        *first = 0;
        for (int i = 1; i < 100; ++i) {
            int* next = (int*) pool.alloc(sizeof(int));
            *next = i;
        }
        for (int i = 0; i < 100; ++i) {
            testAssert(first[i] == i);
        }

        pool.freeAll();

        endTest();
    }
    {
        startTest("Varying sized allocations...");

        PoolAllocator pool(210 * sizeof(int)); // 210 = 1 + 2 + ... + 20

        int* pointers[20];
        for (int i = 0; i < 20; ++i) {
            int size = i + 1;
            pointers[i] = (int*) pool.alloc(size * sizeof(int));
            pointers[i][i] = i;
        }
        for (int i = 0; i < 20; ++i) {
            testAssert(pointers[i][i] == i);
        }

        endTest();
    }
}
