#include "test.hpp"

extern "C" {
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
}

#include "EntityTable.hpp"
#include "LuaEntityInterface.hpp"
#include "LuaHelper.hpp"

int main(int argc, char** argv) {
    {
        startTest("Basic functionality...");

        lua_State* L = luaL_newstate();
        luaL_openlibs(L);
        LuaEntityInterface::load(L);

        EntityTable table;
        Component<int> comp;
        table.use(comp);

        LuaEntityInterface::addTable(L, table, "table");
        LuaEntityInterface::addComponent(L, comp, "comp");

        luaL_dostring(L, R"(
            res = {};

            t = table.table;
            c = comp.comp;

            ent = t:create();
            par = t:create();

            t:owns(par, c, true);
            t:parent(ent, par);
            res[1] = t:has(ent, c);  -- true
            res[2] = t:owns(ent, c); -- false

            t:orphan(ent, true);
            res[3] = t:has(ent, c);  -- false;

            t:get(ent, c, 1);
            t:get(par, c, 2);
            t:parent(ent, par);
            res[4] = t:get(ent, c);  -- 1
            res[5] = t:view(ent, c); -- 3

            t:set(ent, c, 4);
            res[6] = t:get(ent, c);  -- 1

            t:apply();
            res[7] = t:get(ent, c);  -- 4
        )");

        testAssert(LuaHelper::getArrayGlobal<bool>(L, "res", 1) == true);
        testAssert(LuaHelper::getArrayGlobal<bool>(L, "res", 2) == false);
        testAssert(LuaHelper::getArrayGlobal<bool>(L, "res", 3) == false);
        testAssert(LuaHelper::getArrayGlobal<int>(L,  "res", 4) == 1);
        testAssert(LuaHelper::getArrayGlobal<int>(L,  "res", 5) == 3);
        testAssert(LuaHelper::getArrayGlobal<int>(L,  "res", 6) == 1);
        testAssert(LuaHelper::getArrayGlobal<int>(L,  "res", 7) == 4);

        lua_close(L);
        endTest();
    }
    {
        startTest("Void overrides...");

        lua_State* L = luaL_newstate();
        luaL_openlibs(L);
        LuaEntityInterface::load(L);

        EntityTable table;
        Component<void> comp;
        table.use(comp);

        LuaEntityInterface::addTable(L, table, "table");
        LuaEntityInterface::addComponent(L, comp, "comp");

        luaL_dostring(L, R"(
            res = {};

            t = table.table;
            c = comp.comp;

            ent = t:create();
            par = t:create();

            t:parent(ent, par);
            t:get(par, c, true);
            res[1] = t:get(ent, c);  -- false
            res[2] = t:view(ent, c); -- true

            t:set(ent, c, true);
            res[3] = t:get(ent, c);  -- false

            t:apply();
            res[4] = t:get(ent, c); -- true
        )");

        testAssert(LuaHelper::getArrayGlobal<bool>(L, "res", 1) == false);
        testAssert(LuaHelper::getArrayGlobal<bool>(L, "res", 2) == true);
        testAssert(LuaHelper::getArrayGlobal<bool>(L, "res", 3) == false);
        testAssert(LuaHelper::getArrayGlobal<bool>(L, "res", 4) == true);

        lua_close(L);
        endTest();
    }
    {
        startTest("Vector specializations...");

        lua_State* L = luaL_newstate();
        luaL_openlibs(L);
        LuaEntityInterface::load(L);

        EntityTable table;
        Component<Vec<3, int>> comp;
        table.use(comp);

        LuaEntityInterface::addTable(L, table, "table");
        LuaEntityInterface::addComponent(L, comp, "comp");

        luaL_dostring(L, R"(
            t = table.table;
            c = comp.comp;

            ent = t:create();
        )");

        lua_getglobal(L, "ent");
        Entity ent = LuaEntityInterface::checkEntity(L, -1);
        lua_pop(L, 1);

        Vec<3, int> pos;
        pos[0] = 1;
        pos[1] = 2;
        pos[2] = 3;

        table.get(ent, comp) = pos;

        luaL_dostring(L, R"(
            pos = t:get(ent, c);
            t:get(ent, c, {4, 5, 6});
        )");

        testAssert(LuaHelper::getArrayGlobal<int>(L, "pos", 1) == 1);
        testAssert(LuaHelper::getArrayGlobal<int>(L, "pos", 2) == 2);
        testAssert(LuaHelper::getArrayGlobal<int>(L, "pos", 3) == 3);

        pos = table.get(ent, comp);
        testAssert(pos[0] == 4);
        testAssert(pos[1] == 5);
        testAssert(pos[2] == 6);

        lua_close(L);
        endTest();
    }
    return 0;
}
