#include "test.hpp"

extern "C" {
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
}

#include <cstring>

#include "LuaHelper.hpp"
#include "harpMath.hpp"

int main(int argc, char** argv) {
    {
        startTest("Push/Check boolean types...");

        lua_State* L = luaL_newstate();

        LuaHelper::push<bool>(L, true);
        LuaHelper::push<bool>(L, false);

        testAssert(LuaHelper::check<bool>(L, 1) == true);
        testAssert(LuaHelper::check<bool>(L, 2) == false);

        lua_pop(L, 2);

        testAssert(lua_gettop(L) == 0);
        lua_close(L);

        endTest();
    }
    {
        startTest("Push/Check integer types...");

        lua_State* L = luaL_newstate();

        LuaHelper::push<char>(L,          1);
        LuaHelper::push<int>(L,           2);
        LuaHelper::push<long>(L,          3);
        LuaHelper::push<unsigned char>(L, 4);
        LuaHelper::push<unsigned int>(L,  5);
        LuaHelper::push<unsigned long>(L, 6);

        testAssert(LuaHelper::check<char>(L,          1) == 1);
        testAssert(LuaHelper::check<int>(L,           2) == 2);
        testAssert(LuaHelper::check<long>(L,          3) == 3);
        testAssert(LuaHelper::check<unsigned char>(L, 4) == 4);
        testAssert(LuaHelper::check<unsigned int>(L,  5) == 5);
        testAssert(LuaHelper::check<unsigned long>(L, 6) == 6);

        lua_pop(L, 6);

        testAssert(lua_gettop(L) == 0);
        lua_close(L);

        endTest();
    }
    {
        startTest("Push/Check float types...");

        lua_State* L = luaL_newstate();

        LuaHelper::push<float>(L,  1.5);
        LuaHelper::push<double>(L, 2.5);

        testAssert(LuaHelper::check<float>(L,  1) == 1.5);
        testAssert(LuaHelper::check<double>(L, 2) == 2.5);

        lua_pop(L, 2);

        testAssert(lua_gettop(L) == 0);
        lua_close(L);

        endTest();
    }
    {
        startTest("Push/Check C strings...");

        lua_State* L = luaL_newstate();

        LuaHelper::push<const char*>(L, "abcdefghijklmnopqrstuvwxyz");
        testAssert(!strcmp(LuaHelper::check<const char*>(L, 1), "abcdefghijklmnopqrstuvwxyz"));

        lua_pop(L, 1);

        testAssert(lua_gettop(L) == 0);
        lua_close(L);

        endTest();
    }
    {
        startTest("Push/Check Vec types...");

        lua_State* L = luaL_newstate();

        Vec<2, bool>  val1;
        Vec<3, int>   val2;
        Vec<4, float> val3;

        val1[0] = false;
        val1[1] = true;

        val2[0] = 1;
        val2[1] = 2;
        val2[2] = 3;

        val3[0] = 1.5;
        val3[1] = 2.5;
        val3[2] = 3.5;
        val3[3] = 4.5;

        LuaHelper::push<Vec<2, bool>>(L,  val1);
        LuaHelper::push<Vec<3, int>>(L,   val2);
        LuaHelper::push<Vec<4, float>>(L, val3);

        testAssert(LuaHelper::check<Vec<2, bool>>(L,  1) == val1);
        testAssert(LuaHelper::check<Vec<3, int>>(L,   2) == val2);
        testAssert(LuaHelper::check<Vec<4, float>>(L, 3) == val3);

        lua_pop(L, 3);

        testAssert(lua_gettop(L) == 0);
        lua_close(L);

        endTest();
    }
    {
        startTest("Global variable getters...");

        lua_State* L = luaL_newstate();

        luaL_dostring(L, R"(
            res1 = 1;
            res2 = "two";

            res3 = {
                3.5,
                "four",
                5
            }

            res = {
                six   = 6;
                seven = 7.5;
            }
        )");

        testAssert(LuaHelper::getGlobal<int>(L,                 "res1") == 1);
        testAssert(!strcmp(LuaHelper::getGlobal<const char*>(L, "res2"), "two"));

        testAssert(LuaHelper::getArrayGlobal<float>(L,               "res3", 1) == 3.5);
        testAssert(!strcmp(LuaHelper::getArrayGlobal<const char*>(L, "res3", 2), "four"));
        testAssert(LuaHelper::getArrayGlobal<int>(L,                 "res3", 3) == 5);

        testAssert(LuaHelper::getTableGlobal<int>(L,   "res", "six")   == 6);
        testAssert(LuaHelper::getTableGlobal<float>(L, "res", "seven") == 7.5);

        testAssert(lua_gettop(L) == 0);
        lua_close(L);

        endTest();
    }
    {
        startTest("Weak references...");

        lua_State* L = luaL_newstate();
        LuaHelper::load(L);

        luaL_dostring(L, R"(
            res = {1};
        )");

        lua_getglobal(L, "res");
        int ref = LuaHelper::weakRef(L);
        LuaHelper::pushWeakRef(L, ref);
        lua_geti(L, -1, 1);
        testAssert(LuaHelper::check<int>(L, -1) == 1);
        lua_pop(L, 2);

        luaL_dostring(L, R"(
            res = nil;
        )");
        lua_gc(L, LUA_GCCOLLECT, 0);

        LuaHelper::pushWeakRef(L, ref);
        testAssert(lua_isnil(L, -1));
        lua_pop(L, 1);

        testAssert(lua_gettop(L) == 0);
        lua_close(L);

        endTest();
    }
}
