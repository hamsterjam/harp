#define MESSAGE_LENGTH 40
#include "test.hpp"

#include <vector>

#include "EntityTable.hpp"

int main(int argc, char** argv) {
    {
        startTest("Store and recall...");

        EntityTable table;

        Component<int> comp1;
        Component<int> comp2;

        table.use(comp1);
        table.use(comp2);

        auto localComp1 = table.local(comp1);

        Entity ent1 = table.create();
        Entity ent2 = table.create();

        testAssert(!table.owns(ent1, comp1));
        testAssert(!table.owns(ent2, comp1));
        testAssert(!table.owns(ent1, comp2));
        testAssert(!table.owns(ent2, comp2));

        table.get(ent1, comp1) = 1;
        table.get(ent2, comp1) = 2;

        table.get(ent1, comp2) = 3;
        table.get(ent2, comp2) = 4;

        testAssert(table.get(ent1, localComp1) == 1);
        testAssert(table.get(ent2, localComp1) == 2);
        testAssert(table.get(ent1, comp2)      == 3);
        testAssert(table.get(ent2, comp2)      == 4);

        testAssert(table.owns(ent1, localComp1));
        testAssert(table.owns(ent2, localComp1));
        testAssert(table.owns(ent1, comp2));
        testAssert(table.owns(ent2, comp2));

        endTest();
    }
    {
        startTest("Basic iterator test...");

        EntityTable table;
        Component<int> comp;
        table.use(comp);

        Entity ent[100];

        for (int i = 0; i < 100; ++i) {
            ent[i] = table.create();
            table.get(ent[i], comp) = i;
        }

        {
            int i = 0;
            for (auto it = table.begin(); it != table.end(); ++it) {
                Entity next = *it;
                testAssert(table.get(next, comp) == i);
                ++i;
            }
        }
        {
            int i = 0;
            for (auto ent : table) {
                testAssert(table.get(ent, comp) == i);
                ++i;
            }
        }

        endTest();
    }
    {
        startTest("Restricted iterator test...");

        EntityTable table;
        Component<int> compIndex;
        Component<int> compEven;
        Component<int> compOdd;
        Component<int> compTriple;

        table.use(compIndex);
        table.use(compEven);
        table.use(compOdd);
        table.use(compTriple);

        Entity ent[100];

        for (int i = 0; i < 100; ++i) {
            ent[i] = table.create();

            table.get(ent[i], compIndex) = i;

            if (i % 2 == 0) table.get(ent[i], compEven)   = i / 2;
            else            table.get(ent[i], compOdd)    = (i - 1) / 2;
            if (i % 3 == 0) table.get(ent[i], compTriple) = i / 3;
        }

        // Find all evens
        for (auto it = table.begin(compEven); it != table.end(); ++it) {
            Entity ent = *it;
            testAssert(table.get(ent, compIndex) % 2 == 0);
        }

        LocalComponent<int> localOdd = table.local(compOdd);

        // Find all odd triples
        for (auto it = table.begin(localOdd, compTriple); it != table.end(); ++it) {
            Entity ent = *it;
            int triple = table.get(ent, compTriple);
            int odd    = table.get(ent, localOdd);

            testAssert(triple * 3 == odd * 2 + 1);
        }

        // Find all even odds (should be empty)
        for (auto it = table.begin(compEven, localOdd); it != table.end(); ++it) {
            Entity ent = *it;
            testAssert(false);
        }

        endTest();
    }
    {
        startTest("Delayed set...");

        EntityTable table;
        Component<int> comp;
        table.use(comp);

        Entity ents[100];

        for (int i = 0; i < 100; ++i) {
            ents[i] = table.create();
            table.get(ents[i], comp) = 1;
            table.set(ents[i], comp, i);
        }

        for (int i = 0; i < 100; ++i) {
            testAssert(table.get(ents[i], comp) == 1);
        }

        table.apply();

        auto localComp = table.local(comp);
        for (int i = 0; i < 100; ++i) {
            testAssert(table.get(ents[i], localComp) == i);
        }

        endTest();
    }
    {
        startTest("Void overloads...");

        EntityTable table;
        Component<void> comp;
        table.use(comp);

        Entity ents[100];

        for (int i = 0; i < 100; ++i) {
            ents[i] = table.create();
            if (i % 2 == 0) {
                table.set(ents[i], comp, true);
            }
        }

        for (int i = 0; i < 100; ++i) {
            testAssert(!table.get(ents[i], comp))
        }

        table.apply();

        for (int i = 0; i < 100; ++i) {
            testAssert(table.get(ents[i], comp) == (i % 2 == 0));
        }

        endTest();
    }
    {
        startTest("Component removal...");

        EntityTable table;
        Component<void> comp;
        table.use(comp);

        Entity ents[100];
        for (int i = 0; i < 100; ++i) {
            ents[i] = table.create();
            table.get(ents[i], comp) = true;
        }

        for (int i = 0; i < 100; ++i) {
            testAssert(table.get(ents[i], comp));
        }

        for (int i = 1; i < 100; i += 2) {
            table.owns(ents[i], comp) = false;
        }

        for (int i = 0; i < 100; ++i) {
            testAssert(table.get(ents[i], comp) == (i % 2 == 0));
        }

        endTest();
    }
    {
        startTest("Basic parenting...");

        EntityTable table;
        Component<void> comp;
        table.use(comp);

        Entity entA = table.create();
        Entity entB = table.create();
        Entity entC = table.create();

        testAssert(table.orphan(entA));
        testAssert(table.orphan(entB));
        testAssert(table.orphan(entC));

        table.parent(entB) = entA;
        table.parent(entC) = entB;

        testAssert( table.orphan(entA));
        testAssert(!table.orphan(entB));
        testAssert(!table.orphan(entC));

        table.owns(entA, comp) = true;

        testAssert(table.has(entA, comp));
        testAssert(table.has(entB, comp));
        testAssert(table.has(entC, comp));

        endTest();
    }
    {
        startTest("Parented values (default fold)...");

        EntityTable table;
        Component<int> comp;
        table.use(comp);

        Entity par[10];
        Entity child[10][10];
        for (int i = 0; i < 10; ++i) {
            par[i] = table.create();
            table.get(par[i], comp) = i;

            for (int j = 0; j < 10; ++j) {
                child[i][j] = table.create();
                table.parent(child[i][j]) = par[i];
                table.get(child[i][j], comp) = j;
            }
        }

        for (int i = 0; i < 10; ++i) {
            for (int j = 0; j < 10; ++j) {
                testAssert(table.view(child[i][j], comp) == i + j);
            }
        }

        endTest();
    }
    {
        startTest("Parented values (custom fold)...");

        int (*op)(int, int) = [](int a, int b) -> int {
            return a * b;
        };

        EntityTable table;
        Component<int> comp(op);
        table.use(comp);

        Entity par[10];
        Entity child[10][10];
        for (int i = 0; i < 10; ++i) {
            par[i] = table.create();
            table.get(par[i], comp) = i;

            for (int j = 0; j < 10; ++j) {
                child[i][j] = table.create();
                table.parent(child[i][j]) = par[i];
                table.get(child[i][j], comp) = j;
            }
        }

        for (int i = 0; i < 10; ++i) {
            for (int j = 0; j < 10; ++j) {
                testAssert(table.view(child[i][j], comp) == i * j);
            }
        }

        endTest();
    }
    {
        startTest("Parent chain (default fold)..");

        EntityTable table;
        Component<int> comp;
        table.use(comp);

        int result = 0;

        Entity ents[100];
        ents[0] = table.create();
        for (int i = 1; i < 100; ++i) {
            ents[i] = table.create();
            table.parent(ents[i]) = ents[i - 1];

            if (i % 2 == 1) {
                table.get(ents[i], comp) = i;
                result += i;
            }
        }

        testAssert(table.view(ents[99], comp) == result);

        endTest();
    }
    {
        startTest("Parent chain (custom fold)...");

        int (*op)(int, int) = [](int a, int b) -> int {
            return a * b;
        };

        EntityTable table;
        Component<int> comp(op);
        table.use(comp);

        int result = 1;

        Entity ents[100];
        ents[0] = table.create();
        for (int i = 1; i < 100; ++i) {
            ents[i] = table.create();
            table.parent(ents[i]) = ents[i - 1];

            if (i % 2 == 1) {
                table.get(ents[i], comp) = i;
                result *= i;
            }
        }

        testAssert(table.view(ents[99], comp) == result);

        endTest();
    }
    {
        startTest("Parent chain (no addition)...");

        struct Foo { int value; };

        Foo (*op)(Foo, Foo) = [](Foo a, Foo b) -> Foo {
            return Foo {a.value + b.value};
        };

        EntityTable table;
        Component<Foo> comp(op);
        table.use(comp);

        int result = 0;

        Entity ents[100];
        ents[0] = table.create();
        for (int i = 1; i < 100; ++i) {
            ents[i] = table.create();
            table.parent(ents[i]) = ents[i - 1];

            if (i % 2 == 1) {
                table.get(ents[i], comp) = Foo {i};
                result += i;
            }
        }

        testAssert(table.view(ents[99], comp).value == result);

        endTest();
    }
    return 0;
}
